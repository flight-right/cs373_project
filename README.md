# README

Group Members (GitLab ID, EID):

- Lucas Gates: LucasG234, lg34756
- Minho Lee: minMaxLee, ml45895
- Yash Patil: YashPat, ymp238
- Pranav Rayudu: PranavRayudu, tr24427
- Craig Leeson: craig.leeson, cal4373

Git SHA: 25ad302513a26bec51676385b52c02cc419494e9

Project Leader: Craig Leeson

GitLab Pipelines: [Pipelines · Flight Right / cs373_project · GitLab](https://gitlab.com/flight-right/cs373_project/-/pipelines)

Website: [https://www.flightright.me](https://www.flightright.me/)

Estimated completion time for each member:

- Lucas: 6
- Minho: 3
- Yash: 5
- Pranav: 15
- Craig: 5

Actual completion time for each member:

- Lucas: 5
- Minho: 1
- Yash: 5
- Pranav: 10
- Craig: 5

Comments:
- Ran into a last minute issue regarding our frontend build pipeline, something is wrong with our node version. Other than that, everything works including the visualizations (we have all 6 specs).
