from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine, Column, String, Integer
import urllib
import json

from sqlalchemy.sql.sqltypes import NULLTYPE
import keys
from models import db, Airline

import requests
import re


app = Flask(__name__)
app.debug = True

app.config['SQLALCHEMY_DATABASE_URI'] = keys.SQLALCHEMY_DATABASE_URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db.app = app
db.init_app(app)

db.create_all()

airline_list = []

offset = 0
request_url = "http://api.aviationstack.com/v1/airlines?access_key=" + keys.aviation_stack_key 
req  = "http://api.aviationstack.com/v1/airlines?access_key=" + keys.aviation_stack_key 

r = requests.get(request_url)
# r = urllib.request.urlopen(request_url)

if r.status_code != 200: 
    print('exit with', r.status_code)



data = r.json()
while len(data['data']) > 0:

    for item in data['data']:
        # if item["country_name"] == "United States":

        Airline_Name = item["airline_name"] 
        Airline_Country = item["country_name"]
        Airline_Founded =item["date_founded"]
        Airline_Av_Fleet = item["fleet_average_age"]
        Airline_Fleet_Size = item["fleet_size"]
        Airline_Iata= item["iata_code"]
        Airline_Icao = item["icao_code"]
        
        # airline's iata code -> get name using database? 
        Airline_Hub_Code = item["hub_code"]

        

        # only add airline data if important data are not null
        new_airline = None
        if Airline_Name and Airline_Country and Airline_Founded and Airline_Av_Fleet  and Airline_Fleet_Size \
             and Airline_Iata and Airline_Icao: 


            print("---", Airline_Name, "-----")

            # check that media data is available
            news_url = "http://api.mediastack.com/v1/news?access_key=" + keys.media_stack_key
            news_param = {"keywords" : re.sub(r'[^\w\s]', '', Airline_Name), "countries" : "us"}
            response = requests.get(news_url, params=news_param)
            news_exists = False
            news_description = None 
            news_url = None 
            news_image = None 

            print('finding news')

            if response.status_code == 200: 
                # successful api call
                r_json = response.json()
                news_data = r_json["data"] if "data" in r_json else None 
                i = 0
                length = min(len(news_data), 20)
                while not news_exists and i < length:
                    # find the article in json that has appropriate media data 
                    if news_data and news_data[i]: 
                        if "description" in news_data[i] and "url" in news_data[i] and "image" in news_data[i] \
                            and news_data[i]["description"] != None and news_data[i]["url"] != None and news_data[i]["image"] != None:
                            # add this to new_airline
                            news_description = news_data[i]["description"] 
                            news_url = news_data[i]["url"] 
                            news_image = news_data[i]["image"] 
                            news_exists = True
                            break
                    i += 1
            else: 
                print('news not found', response.status_code)
            
            print('finding logo')


            # checking for airline logo data 
            logo = None
            query = "https://autocomplete.clearbit.com/v1/companies/suggest?query=" + Airline_Name
            r = requests.get(query)
            if r.status_code == 200: 
                # logo exists 
                if len(r.json()) > 0 and 'logo' in r.json()[0]:
                    logo = r.json()[0]['logo']
                    print('logo found')
            if not logo: 
                print('using bing because', r.status_code)
                # logo not found with first api, so use bing img 
                bing_url = "https://bing-image-search1.p.rapidapi.com/images/search"
                querystring = {"q": Airline_Name + " logo"}
                response = requests.request("GET", bing_url, headers=keys.bing_headers, params=querystring)
                r_json = response.json()
                if "value" in r_json and len(r_json["value"]) > 0: 
                    for i in range(len(r_json["value"])):
                        if "thumbnailUrl" in r_json["value"][i]: 
                            logo = r_json["value"][i]["thumbnailUrl"]
                            break          
                else:
                    print('logo not found', response.status_code)   

            if news_exists or logo :
                print('sufficient', Airline_Name)
                # all text data and at least one media data exists 
                new_airline = Airline(airline_name = item["airline_name"],  airline_country= item["country_name"], airline_founded=item["date_founded"], \
                                airline_av_fleet = item["fleet_average_age"], airline_fleet_size = item["fleet_size"], airline_iata= item["iata_code"],  \
                                airline_icao = item["icao_code"], airline_hub_code = item["hub_code"], \
                                airline_logo = logo, airline_news_text = news_description, airline_news_img = news_image, airline_news_url = news_url) 
            else: 
                print('insufficient', Airline_Name)
            

        if new_airline: 
            airline_list.append(new_airline)

    # break if more than 100 airlines
    if len(airline_list) >= 100:
        break

    offset += 100
    offset_request = request_url + "&offset=" + str(offset) 
    r = requests.get(offset_request)
    # r = urllib.request.urlopen(offset_request)
    if r.status_code != 200: 
        # something went wrong 
        print('exit with', r.status_code)
        break
    data = r.json()


print(len(airline_list))

db.session.add_all(airline_list)
db.session.commit()
