from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine, Column, String, Integer
import urllib
import json
import keys
from models import *
import requests

app = Flask(__name__)
app.debug = True

app.config["SQLALCHEMY_DATABASE_URI"] = keys.SQLALCHEMY_DATABASE_URI
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db.app = app
db.init_app(app)

db.create_all()

flights_list = []


def parse_flights(data):
    for item in data["data"]:
        flight_number = item["flight"]["number"]
        duplicate_flight = Flight.query.filter_by(flight_number=flight_number).first()

        if duplicate_flight == None:
            # aircraft image()
            if item["aircraft"] != None:
                aircraft_reg = item["aircraft"]["registration"]
                url = (
                    "https://aerodatabox.p.rapidapi.com/aircrafts/reg/"
                    + aircraft_reg
                    + "/image/beta"
                )

                response = requests.request(
                    "GET", url, headers=keys.aero_data_box_headers
                )
                aircraft_img = "No image"
                if response.status_code == 200:
                    # image exists
                    aircraft_img = response.json()["url"]
            else:
                aircraft_img = "No image"

            if item["live"] != None:
                flight_lat = item["live"]["latitude"]
                flight_long = item["live"]["longitude"]
            else:
                flight_lat = -1
                flight_long = -1

            new_flight = Flight(
                flight_number=item["flight"]["number"],
                flight_airline_name=item["airline"]["name"],
                flight_dept_time=item["departure"]["scheduled"],
                flight_arr_time=item["arrival"]["scheduled"],
                flight_dept_airport=item["departure"]["airport"],
                flight_arr_airport=item["arrival"]["airport"],
                flight_status=item["flight_status"],
                flight_lat=flight_lat,
                flight_long=flight_long,
                flight_dept_airport_iata=item["departure"]["iata"],
                flight_dept_airport_icao=item["departure"]["icao"],
                flight_arr_airport_iata=item["arrival"]["iata"],
                flight_arr_airport_icao=item["arrival"]["icao"],
                flight_iata=item["flight"]["iata"],
                flight_icao=item["flight"]["icao"],
                flight_aircraft_img=aircraft_img,
            )

            # Find airport and airline ids if they exist

            try:
                # Note: First 3 characters of flight icao should be airline airline_icao
                if new_flight.flight_icao == None:
                    airline_icao = None
                else:
                    airline_icao = new_flight.flight_icao[0:3]
                airport_icao = new_flight.flight_dept_airport_icao
                if airline_icao != None:
                    airline = Airline.query.filter_by(airline_icao=airline_icao).first()
                    if airline != None:
                        new_flight.airline = airline
                        airline.flights.append(new_flight)
                if airport_icao != None:
                    airport = Airport.query.filter_by(airport_icao=airport_icao).first()
                    if airport != None:
                        new_flight.airport = airport
                        airport.flights.append(new_flight)

                # Link together the airport and airline because they share a flight
                if (
                    airline_icao != None
                    and airport_icao != None
                    and airline != None
                    and airport != None
                ):
                    airline.airports.append(airport)
                    airport.airlines.append(airline)
            except Exception as e:
                print("Failed to search for airline or aiport")
                print(e)

            flights_list.append(new_flight)


airlines = Airline.query.all()
airports = Airport.query.all()

for airline in airlines:

    if airline.airline_icao != None:
        request_url = (
            "http://api.aviationstack.com/v1/flights?access_key="
            + keys.aviation_stack_key
            + "&airline_icao="
            + airline.airline_icao
            + "&limit=10"
        )
        r = urllib.request.urlopen(request_url)
        data = json.loads(r.read())

        parse_flights(data)
        print("finished airline " + str(len(flights_list)))

for airport in airports:

    if airport.airport_icao != None:
        request_url = (
            "http://api.aviationstack.com/v1/flights?access_key="
            + keys.aviation_stack_key
            + "&dep_icao="
            + airport.airport_icao
            + "&limit=10"
        )
        r = urllib.request.urlopen(request_url)
        data = json.loads(r.read())

        parse_flights(data)
        print("finished airport " + str(len(flights_list)))

print(len(flights_list))
db.session.add_all(flights_list)
db.session.commit()
