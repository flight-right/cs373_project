from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine, Column, String, Integer
import urllib
import json
import keys
from models import db, Airport
import requests

app = Flask(__name__)
app.debug = True

app.config["SQLALCHEMY_DATABASE_URI"] = keys.SQLALCHEMY_DATABASE_URI
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db.app = app
db.init_app(app)

db.create_all()

airport_list = []

offset = 0
request_url = (
    "http://api.aviationstack.com/v1/airports?access_key=" + keys.aviation_stack_key
)
r = urllib.request.urlopen(request_url)
data = json.loads(r.read())

while len(data["data"]) > 0:

    for item in data["data"]:
        # get airports in USA only
        if item["country_name"] == "United States":
            if item["iata_code"] != "" and item["icao_code"] != "":
                # request json for other features

                new_airport = Airport(
                    airport_name=item["airport_name"],
                    airport_city="NaN",
                    airport_flight_volume=0,
                    airport_lat=item["latitude"],
                    airport_long=item["longitude"],
                    airport_rating=0.0,
                    airport_address="NaN",
                    airport_phone_number=item["phone_number"],
                    airport_website="NaN",
                    airport_runway_number=0,
                    airport_timezone=item["gmt"],
                    airport_iata=item["iata_code"],
                    airport_icao=item["icao_code"],
                )

                print("airport", item["airport_name"])

                # airport news
                query = (
                    "icao/" + item["icao_code"] if item["icao_code"] != None else None
                )
                if query is None:
                    print("none icao", end=" ")
                    query = (
                        "iata/" + item["iata_code"]
                        if item["iata_code"] != None
                        else None
                    )
                if query is not None:
                    url = "https://aerodatabox.p.rapidapi.com/airports/" + query
                    response = requests.request(
                        "GET", url, headers=keys.aero_data_box_headers
                    )
                    if response.status_code == 200:
                        r_json = response.json()
                        new_airport.airport_twitter = (
                            r_json["urls"]["twitter"]
                            if "twitter" in r_json["urls"]
                            else None
                        )
                        new_airport.airport_maps = (
                            r_json["urls"]["googleMaps"]
                            if "googleMaps" in r_json["urls"]
                            else None
                        )
                        new_airport.airport_website = (
                            r_json["urls"]["webSite"]
                            if "webSite" in r_json["urls"]
                            else None
                        )
                        print(
                            "adding",
                            new_airport.airport_twitter,
                            new_airport.airport_maps,
                            new_airport.airport_website,
                        )
                else:
                    # don't add any airport news
                    pass
                airport_list.append(new_airport)

    if len(airport_list) >= 100:
        break

    offset += 100
    offset_request = request_url + "&offset=" + str(offset)
    r = urllib.request.urlopen(offset_request)
    data = json.loads(r.read())

print(len(airport_list))
db.session.add_all(airport_list)
db.session.commit()
