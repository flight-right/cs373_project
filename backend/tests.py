from unittest import main, TestCase
from app import app

import os
from dotenv import load_dotenv

class Tests(TestCase):


	# Test our environmental variables
	def test_dot_env(self):
		load_dotenv()

		sql_database_uri = os.getenv("SQLALCHEMY_DATABASE_URI")
		self.assertNotEqual(sql_database_uri, None)
	
	# Test endpoint /status
	def test_status(self):
		test_client = app.test_client()
		r = test_client.get("/status")
		self.assertEqual(r.status_code, 200)

		data = r.json
		self.assertEqual(data, "Everything ok")

	# Test endpoint /airlines
	def test_airlines(self):
		test_client = app.test_client()
		r = test_client.get("/airlines")
		self.assertEqual(r.status_code, 200)

		data = r.json
		# This is our defined minimum number of airlines in the database
		self.assertTrue(data["count"] >= 100)
		self.assertEqual(data["count"], len(data["data"]))

	# Test endpoint /airports
	def test_airports(self):
		test_client = app.test_client()
		r = test_client.get("/airports")
		self.assertEqual(r.status_code, 200)

		data = r.json
		# This is our defined minimum number of airlines in the database
		self.assertTrue(data["count"] >= 100)
		self.assertEqual(data["count"], len(data["data"]))

	# Test endpoint /flights
	def test_flights(self):
		test_client = app.test_client()
		r = test_client.get("/flights")
		self.assertEqual(r.status_code, 200)

		data = r.json
		# This is our defined minimum number of airlines in the database
		self.assertTrue(data["count"] >= 100)
		self.assertEqual(data["count"], len(data["data"]))


	# Test endpoint /airlines/<id>
	def test_airlines_id(self):
		test_client = app.test_client()
		r = test_client.get("/airlines/1")
		self.assertEqual(r.status_code, 200)

		data = r.json
		data_fields = ["airline_av_fleet", "airline_country", "airline_fleet_size", "airline_founded", "airline_hub_code", \
						"airline_iata", "airline_icao", "airline_logo", "airline_name", "airline_news_img", "airline_news_text", \
						"airline_news_url", "airports", "flights", "id"]

		self.assertEqual(len(data), len(data_fields))
		for field in data_fields:
			self.assertTrue(field in data)
		self.assertEqual(data["id"], 1)

	# Test endpoint /airports/<id>
	def test_airports_id(self):
		test_client = app.test_client()
		r = test_client.get("/airports/1")
		self.assertEqual(r.status_code, 200)

		data = r.json
		data_fields = ["airlines", "airport_address", "airport_city", "airport_flight_volume", "airport_iata", "airport_icao", \
						"airport_lat", "airport_long", "airport_maps", "airport_name", "airport_phone_number", "airport_rating", \
						"airport_runway_number", "airport_state", "airport_timezone", "airport_twitter", "airport_website", \
						"flights", "id"]

		self.assertEqual(len(data), len(data_fields))
		for field in data_fields:
			self.assertTrue(field in data)
		self.assertEqual(data["id"], 1)

	# Test endpoint /flights/<id>
	def test_flights_id(self):
		test_client = app.test_client()
		r = test_client.get("/flights/1")
		self.assertEqual(r.status_code, 200)

		data = r.json
		data_fields = ["airline", "airport", "flight_aircraft_img", "flight_airline_name", "flight_arr_airport", \
						"flight_arr_airport_iata", "flight_arr_airport_icao", "flight_arr_time", "flight_dept_airport", \
						"flight_dept_airport_iata", "flight_dept_airport_icao", "flight_dept_time", "flight_iata", "flight_icao", \
						"flight_lat", "flight_long", "flight_number", "flight_status", "id"]

		self.assertEqual(len(data), len(data_fields))
		for field in data_fields:
			self.assertTrue(field in data)
		self.assertEqual(data["id"], 1)

	# Test enpoint /search/terms
	def test_search(self):
		test_client = app.test_client()
		r = test_client.get("/search/A")
		self.assertEqual(r.status_code, 200)

		data = r.json
		# Ensure we have results (No guaranteed minimum)
		self.assertTrue(data["count"] >= 0)
		# One list per model
		self.assertEqual(len(data["data"]), 3)

if __name__ == "__main__":
    main()