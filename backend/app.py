from flask import Flask, jsonify, make_response, request
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import flask
from marshmallow import fields
from flask_cors import CORS
from sqlalchemy import or_, asc, desc, cast, create_engine, Column, String, Integer
import sqlalchemy.sql.sqltypes as types
import json
import os
from dotenv import load_dotenv
from models import *
import math

app = Flask(__name__)
CORS(app)
app.debug = True

load_dotenv()
app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv("SQLALCHEMY_DATABASE_URI")
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

ma = Marshmallow(app)

class FlatAirlineSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Airline


class FlatAirportSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Airport


class FlatFlightSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Flight


class AirlineSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Airline

    airports = ma.Nested(FlatAirportSchema, many=True)
    flights = ma.Nested(FlatFlightSchema, many=True)


class AirportSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Airport

    airlines = ma.Nested(FlatAirlineSchema, many=True)
    flights = ma.Nested(FlatFlightSchema, many=True)


class FlightSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Flight

    airline = ma.Nested(FlatAirlineSchema)
    airport = ma.Nested(FlatAirportSchema)


airlines_schema = AirlineSchema(many=True)
airports_schema = AirportSchema(many=True)
flights_schema = FlightSchema(many=True)

single_airline_schema = AirlineSchema()
single_airport_schema = AirportSchema()
single_flight_schema = FlightSchema()

airline_filter_dict = {"country": "airline_country"}
airport_filter_dict = {"city": "airport_city", "state": "airport_state", 
                        "timezone": "airport_timezone"}
flight_filter_dict = {"number": "flight_number", "departure_airport": 
                        "flight_dept_airport", "arrival_airport": "flight_arr_airport", 
                        "status": "flight_status"}

airline_sort_dict = {"id": "id", "name": "airline_name", "country": "airline_country",
                        "founded": "airline_founded", "fleet_size": "airline_fleet_size"}
airport_sort_dict = {"id": "id", "name": "airport_name", "city": "airport_city",
                        "state": "airport_state", "flight_volume": "airport_flight_volume",
                        "rating": "airport_rating"}
flight_sort_dict = {"id": "id", "depature_time": "flight_dept_time", "arrival_time": "flight_arr_time",
                        "number": "flight_number"}

# return paginated view and its count (<= limit)
# (Actually call for objects is done here, and result is returned)
def pagination_handler(params, query_instances, schema):
    total = query_instances.count()
    page_len = int(params["limit"]) if "limit" in params else 100
    page_number = int(params["page"]) if "page" in params else 1
    total_pages = math.ceil(total / page_len)
    if page_number < 1: page_number = 1
    if page_number > total_pages: page_number = total_pages
    page_instances = query_instances.paginate(page_number, page_len, False)
    return schema.dump(page_instances.items), len(page_instances.items)

# return query with filtering applied
def filter_handler(params, query_instances, model, filter_dict):
    for filter in filter_dict.keys():
        if filter in params.keys():
            condition = []
            col = getattr(model, filter_dict[filter])
            for value in params[filter].split(","):
                condition.extend([col.ilike(value)])
            query_instances = query_instances.filter(or_(*condition))
    return query_instances

# return query with sorting applied
def sort_handler(params, query_instances, model, sort_dict):
    if "sort_by" in params.keys():
        sorting_param = params["sort_by"]
        sorting_col = sort_dict[sorting_param]
        if "reverse" in params.keys():
            query_instances = query_instances.order_by(desc(sorting_col))
        else:
            query_instances = query_instances.order_by(asc(sorting_col))
    return query_instances

# return query with search applied
def search_handler(params, query_instances, model):
    if "search_term" in params.keys():
        search_term = params["search_term"]
        condition = [
                cast(column, types.String).ilike(f"%{search_term}%")
                for column in model.__table__.columns
            ]
        query_instances = query_instances.filter(or_(*condition))
    return query_instances

@app.route("/")
def hello_world():
    return '<img src="https://i.kym-cdn.com/photos/images/original/001/211/814/a1c.jpg" alt="cowboy" />'


@app.route("/airlines", methods=["GET"])
def get_airlines():
    airlines = Airline.query
    total = airlines.count()
    params = request.args.to_dict()

    filtered = filter_handler(params, airlines, Airline, airline_filter_dict)
    searched = search_handler(params, filtered, Airline)
    sorted = sort_handler(params, searched, Airline, airline_sort_dict)
    output, count = pagination_handler(params, sorted, airlines_schema)

    return jsonify({"total": total, "count": count, "data": output})


@app.route("/airports", methods=["GET"])
def get_airports():
    airports = Airport.query
    total = airports.count()
    params = request.args.to_dict()

    filtered = filter_handler(params, airports, Airport, airport_filter_dict)
    searched = search_handler(params, filtered, Airport)
    sorted = sort_handler(params, searched, Airport, airport_sort_dict)
    output, count = pagination_handler(params, sorted, airports_schema)

    return jsonify({"total": total, "count": count, "data": output})


@app.route("/flights", methods=["GET"])
def get_flights():
    flights = Flight.query
    total = flights.count()
    params = request.args.to_dict()

    filtered = filter_handler(params, flights, Flight, flight_filter_dict)
    searched = search_handler(params, filtered, Flight)
    sorted = sort_handler(params, searched, Flight, flight_sort_dict)
    output, count = pagination_handler(params, sorted, flights_schema)
    
    return jsonify({"total": total, "count": count, "data": output})


@app.route("/airlines/<id>", methods=["GET"])
def get_airline_id(id):
    airline = Airline.query.get(id)
    if airline is None:
        response = flask.Response(
            json.dumps({"error": "No Airline with id '" + id + "' present"}),
            mimetype="application/json",
        )
        response.status_code = 404
        return response
    return single_airline_schema.jsonify(airline)


@app.route("/airports/<id>", methods=["GET"])
def get_airport_id(id):
    airport = Airport.query.get(id)
    if airport is None:
        response = flask.Response(
            json.dumps({"error": "No Airport with id '" + id + "' present"}),
            mimetype="application/json",
        )
        response.status_code = 404
        return response
    return single_airport_schema.jsonify(airport)


@app.route("/flights/<id>", methods=["GET"])
def get_flight_id(id):
    flight = Flight.query.get(id)
    if flight is None:
        response = flask.Response(
            json.dumps({"error": "No Flight with id '" + id + "' present"}),
            mimetype="application/json",
        )
        response.status_code = 404
        return response
    return single_flight_schema.jsonify(flight)


@app.route("/status", methods=["GET"])
def get_status():
    return jsonify("Everything ok")

@app.route("/search/<terms>", methods=["GET"])
def get_search(terms):
    params = request.args.to_dict()
    models = {Airport: airports_schema, Airline: airlines_schema, Flight: flights_schema}
    output = []
    count = 0
    for model in models.keys():
        queryInstances = model.query
        condition = [
                cast(column, types.String).ilike(f"%{terms}%")
                for column in model.__table__.columns
            ]
        queryInstances = queryInstances.filter(or_(*condition))
        result = pagination_handler(params, queryInstances, models[model])
        output.append(result[0])
        count += result[1]
    # total return
    return jsonify({"count": count, "data": output})

db.init_app(app)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
