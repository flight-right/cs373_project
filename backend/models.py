from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import backref


db = SQLAlchemy()

airline_to_airport = db.Table('airline_to_airport',
    db.Column('airline_id', db.Integer(), db.ForeignKey('airline.id')),
    db.Column('airport_id', db.Integer(), db.ForeignKey('airport.id'))
)

class Airline(db.Model):
    id = db.Column(db.Integer, primary_key = True)

    airline_name = db.Column(db.String())
    airline_country = db.Column(db.String())
    airline_founded = db.Column(db.Integer())
    airline_av_fleet = db.Column(db.Float())
    airline_fleet_size = db.Column(db.Integer())

    airline_iata = db.Column(db.String())
    airline_icao = db.Column(db.String())
    airline_hub_code = db.Column(db.String())

    #media 
    airline_logo = db.Column(db.String())
    airline_news_text = db.Column(db.String())
    airline_news_img = db.Column(db.String())
    airline_news_url = db.Column(db.String())
    
    flights = db.relationship('Flight', backref='airline')
    airports = db.relationship('Airport', secondary = airline_to_airport, overlaps='airlines')

class Airport(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    
    airport_name = db.Column(db.String())
    airport_city = db.Column(db.String())
    airport_state = db.Column(db.String())
    airport_flight_volume = db.Column(db.Integer())
    airport_lat = db.Column(db.Float())
    airport_long = db.Column(db.Float())
    airport_rating = db.Column(db.Float())
    airport_address = db.Column(db.String())
    airport_phone_number = db.Column(db.String())
    airport_website = db.Column(db.String())
    airport_runway_number = db.Column(db.Integer())
    airport_timezone = db.Column(db.String())
    
    #media 
    airport_twitter = db.Column(db.String())
    airport_maps = db.Column(db.String())

    airport_iata = db.Column(db.String())
    airport_icao = db.Column(db.String())

    flights = db.relationship('Flight', backref='airport')
    airlines = db.relationship('Airline', secondary = airline_to_airport, overlaps='airports')


class Flight(db.Model):
    id = db.Column(db.Integer, primary_key = True)

    flight_number = db.Column(db.String())
    flight_airline_name = db.Column(db.String())
    flight_dept_time = db.Column(db.String())
    flight_arr_time = db.Column(db.String())
    flight_dept_airport = db.Column(db.String())
    flight_arr_airport = db.Column(db.String())
    flight_status = db.Column(db.String())

    flight_dept_airport_iata = db.Column(db.String())
    flight_dept_airport_icao = db.Column(db.String())
    flight_arr_airport_iata = db.Column(db.String())
    flight_arr_airport_icao = db.Column(db.String())
    flight_iata = db.Column(db.String())
    flight_icao = db.Column(db.String())

    #media 
    flight_aircraft_img = db.Column(db.String())
    flight_lat = db.Column(db.Float())
    flight_long = db.Column(db.Float())

    airline_id = db.Column(db.Integer(), db.ForeignKey('airline.id'))
    airport_id = db.Column(db.Integer(), db.ForeignKey('airport.id'))
