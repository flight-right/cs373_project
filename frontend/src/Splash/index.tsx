import React from 'react';
import Box from '@mui/material/Box';
import {Container, Grid, Typography} from '@mui/material';
import {green, orange, purple} from '@mui/material/colors';

import FlightIcon from '@mui/icons-material/Flight';
import FlightTakeoffIcon from '@mui/icons-material/FlightTakeoff';
import AirplaneTicketIcon from '@mui/icons-material/AirplaneTicket';

import CardComponent from './components/pageCard';

function Hero() {
  return (
    <Box
      sx={{
        minHeight: 400,
        display: 'flex',
        alignItems: 'center',
      }}
    >
      <Container>
        <Typography
          variant={'h1'}
          sx={{
            fontWeight: 'bold',
          }}
        >
          Perfect Flight Data
        </Typography>
        <Typography variant={'h6'}>All Day, Every Day.</Typography>
      </Container>
    </Box>
  );
}

function Splash() {
  return (
    <>
      <Hero />
      <Container sx={{mt: 4, mb: 4}}>
        <Grid container spacing={4}>
          <CardComponent
            title={'Flights'}
            icon={<FlightIcon />}
            text={'See data on all current and upcoming flights.'}
            buttonText={'View Flights'}
            href={'/flights'}
            color={purple[400]}
          />
          <CardComponent
            title={'Airports'}
            icon={<FlightTakeoffIcon />}
            text={
              'See data on airport traffic, weather, and other information.'
            }
            buttonText={'View Airports'}
            href={'/airports/limit=12&page=1'}
            color={green[600]}
          />
          <CardComponent
            title={'Airlines'}
            icon={<AirplaneTicketIcon />}
            text={'See all airline companies, their flights, and news.'}
            buttonText={'View Airlines'}
            href={'/airlines/limit=9&page=1'}
            color={orange[600]}
          />
        </Grid>
      </Container>
    </>
  );
}

export default Splash;
