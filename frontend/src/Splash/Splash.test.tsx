import React from 'react';
import renderer from 'react-test-renderer';
import Splash from './index';

// https://jestjs.io/docs/snapshot-testing
it('splash page renders correctly', () => {
  const tree = renderer.create(<Splash />).toJSON();
  expect(tree).toMatchSnapshot();
});
