import React from 'react';
import {Avatar, Grid, Link, Stack, Typography} from '@mui/material';
import Box from '@mui/material/Box';

interface PageCard {
  title: string;
  text: string;
  buttonText: string;
  href: string;
  color?: string;
  icon?: React.ReactElement;
}

export default function CardComponent(props: PageCard) {
  const {title, text, buttonText, href, color, icon} = props;

  return (
    <Grid item xs={12} sm={4}>
      <Stack direction={{xs: 'row', s: 'column'}} spacing={2}>
        <Box mt={1}>
          <Avatar
            sx={{
              width: 50,
              height: 50,
              bgcolor: color ? color : 'black',
            }}
            variant={'rounded'}
          >
            {icon}
          </Avatar>
        </Box>
        <Box>
          <Typography variant={'h5'} sx={{fontWeight: 'medium'}}>
            {title}
          </Typography>
          <Typography variant="body2">{text}</Typography>
          <Box mt={1}>
            <Link underline="always" color={'#000'} href={href}>
              {buttonText}
            </Link>
          </Box>
        </Box>
      </Stack>
    </Grid>
  );
}
