import React from 'react';
import {render, screen} from '@testing-library/react';
import App from './App';

it('renders header', () => {
  render(<App />);

  // https://stackoverflow.com/questions/57827126/how-to-test-anchors-href-with-react-testing-library
  const titleLink = screen.getByRole('link', {
    name: 'Flight Right Logo Flight Right',
  });
  expect(titleLink).toHaveAttribute('href', '/');

  const flightsLink = screen.getByRole('link', {name: 'Flights'});
  expect(flightsLink).toHaveAttribute('href', '/flights');

  const airportsLink = screen.getByRole('link', {name: 'Airports'});
  expect(airportsLink).toHaveAttribute('href', '/airports/limit=12&page=1');

  const airlinesLink = screen.getByRole('link', {name: 'Airlines'});
  expect(airlinesLink).toHaveAttribute('href', '/airlines/limit=9&page=1');

  const aboutLink = screen.getByRole('link', {name: 'About'});
  expect(aboutLink).toHaveAttribute('href', '/about');
});

// test render footer
it('renders footer', () => {
  render(<App />);

  // https://stackoverflow.com/questions/57827126/how-to-test-anchors-href-with-react-testing-library
  const footerText = screen.getByText('© 2021 Flight Right Project.');
  expect(footerText).toBeInTheDocument();

  // https://stackoverflow.com/questions/57827126/how-to-test-anchors-href-with-react-testing-library
  const repoLink = screen.getByRole('link', {name: 'Repo'});
  expect(repoLink).toHaveAttribute(
    'href',
    'https://gitlab.com/flight-right/cs373_project'
  );

  const apiLink = screen.getByRole('link', {name: 'API Docs'});
  expect(apiLink).toHaveAttribute(
    'href',
    'https://documenter.getpostman.com/view/17755692/UUy4bjoc'
  );
});
