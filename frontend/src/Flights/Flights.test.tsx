import React from 'react';
import {render, screen} from '@testing-library/react';
import {shallow} from 'enzyme';
import {Flight} from './index';
import FlightCard from './components/flightCard';
import FlightDataDisplay from './components/flightDataDisplay';
import moment from 'moment';

const flightData: Flight = {
  airline: {
    airline_name: 'string',
    airline_country: 'string',
    airline_iata: 'string',
    id: 0,
  },
  arrival_airport: {
    id: 0,
    name: "O'Hare International Airport",
    code: 'OHA',
    lat: 82.44,
    lon: 11.45,
  },
  arrival_time: moment('2021-10-26T16:56:02Z'),
  departure_airport: {
    id: 0,
    name: 'Los Angeles Airport',
    code: 'LAX',
    lat: 45.434,
    lon: 21.45,
  },
  departure_time: moment('2021-10-26T16:59:55Z'),
  duration: moment('2021-10-26T16:56:02Z').diff(
    moment('2021-10-26T16:59:55Z'),
    'minutes'
  ),
  flight_number: '',
  id: 549,
  status: 'On Time',
};

it('flight display renders correct data', () => {
  const wrapper = shallow(<FlightDataDisplay rows={[flightData]} />);
  expect(wrapper.find(FlightCard)).toHaveLength(1);
});

it('flightCard renders correct data', () => {
  render(<FlightCard {...flightData} />);

  const instanceLink = screen.getByRole('link');
  expect(instanceLink).toHaveAttribute('href', `/flights/${flightData.id}`);

  const departure_time = screen.getByText(
    flightData.departure_time.format('dddd, MMMM Do')
  );
  expect(departure_time).toBeInTheDocument();

  const arrival_time = screen.getByText(
    `${flightData.departure_time.format(
      'h:mma'
    )} - ${flightData.arrival_time.format('h:mma')}`
  );
  expect(arrival_time).toBeInTheDocument();

  // const getByText = within(flightCard.te);
  const airport_dept = screen.getByText(flightData.departure_airport.name, {
    exact: false,
  });
  expect(airport_dept).toBeInTheDocument();

  const airport_arr = screen.getByText(flightData.arrival_airport.name, {
    exact: false,
  });
  expect(airport_arr).toBeInTheDocument();
});
