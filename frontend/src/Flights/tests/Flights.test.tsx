import React from 'react';
import Flights from '../index';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route} from 'react-router-dom';
import FlightInstance from '../instance/index';

it('loads model page(s) without issue', () => {
  const div = document.createElement('div');

  ReactDOM.render(
    <BrowserRouter>
      <Route exact path="/flights" component={Flights} />
    </BrowserRouter>,
    div
  );
  expect(div.textContent).not.toBe(null);
});

it('loads instance pages without issue', () => {
  const div = document.createElement('div');

  ReactDOM.render(
    <BrowserRouter>
      <Route exact path="/flights/:id" component={FlightInstance} />
    </BrowserRouter>,
    div
  );
  expect(div.textContent).not.toBe(null);
});

it('loads model page for sorting without issue', () => {
  const div = document.createElement('div');

  ReactDOM.render(
    <BrowserRouter>
      <Route
        exact
        path="/flights?limit=9&page=1&sort_by=departure_time"
        component={Flights}
      />
    </BrowserRouter>,
    div
  );
  expect(div.textContent).not.toBe(null);
});

it('loads model page for filtering without issue', () => {
  const div = document.createElement('div');

  ReactDOM.render(
    <BrowserRouter>
      <Route
        exact
        path="/flights?limit=10&page=1&departure_airport=Shang-Yi"
        component={Flights}
      />
    </BrowserRouter>,
    div
  );
  expect(div.textContent).not.toBe(null);
});

it('loads model page for filtering AND sorting without issue', () => {
  const div = document.createElement('div');

  ReactDOM.render(
    <BrowserRouter>
      <Route
        exact
        path="/airlines?limit=9&page=1&sort_by=departure_time&reverse=true&departure_airport=Shang-Yi"
        component={Flights}
      />
    </BrowserRouter>,
    div
  );
  expect(div.textContent).not.toBe(null);
});
