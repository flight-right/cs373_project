import React from 'react';
import {
  ComposableMap,
  Geographies,
  Geography,
  Marker,
  Line,
  Sphere,
} from 'react-simple-maps';
// import {AirportData} from '../instance';
import {purple} from '@mui/material/colors';
import {Box} from '@mui/material';
import {Airport} from '../index';

const geoUrl =
  'https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/topojson-maps/world-110m.json';

interface MapChartProps {
  from: Airport;
  to: Airport;
}

const MapChart = (props: MapChartProps) => {
  // order should be longitude, latitude
  // https://github.com/zcreativelabs/react-simple-maps/issues/67
  const fromCoord: [number, number] = [props.from.lon, props.from.lat];
  const toCoord: [number, number] = [props.to.lon, props.to.lat];

  const fromName = props.from.name;
  const toName = props.to.name;

  const centerCoord: [number, number] = [
    (fromCoord[0] + toCoord[0]) / 2,
    (fromCoord[1] + toCoord[1]) / 2,
  ];

  return (
    <Box sx={{width: '100%', m: 'auto'}}>
      <ComposableMap
        projection="geoOrthographic"
        projectionConfig={{
          scale: 200,
          rotate: [centerCoord[1], centerCoord[0], 0],
          // center: centerCoord,
        }}
      >
        <Sphere
          fill={'#D9FFFF'}
          id={'rsm-sphere'}
          stroke={'black'}
          strokeWidth={4}
        />
        {/*<Graticule stroke="#DDD" />*/}
        <Geographies
          geography={geoUrl}
          fill="#c7f5bc"
          stroke="#333"
          strokeWidth={0.5}
        >
          {({geographies}) =>
            geographies.map(geo => (
              <Geography key={geo.rsmKey} geography={geo} />
            ))
          }
        </Geographies>
        <Line
          from={fromCoord}
          to={toCoord}
          stroke={purple[400]}
          strokeWidth={4}
          strokeLinecap="round"
        />
        <Marker key={fromName} coordinates={fromCoord}>
          <circle r={5} fill={purple[400]} stroke="#000" strokeWidth={0} />
          <text
            textAnchor="middle"
            y={20}
            fill={purple[400]}
            style={{
              fontFamily: 'system-ui',
              fontSize: '1.2em',
              fontWeight: 'bold',
            }}
          >
            {fromName}
          </text>
        </Marker>
        <Marker key={toName} coordinates={toCoord}>
          <circle r={5} fill={purple[400]} stroke="#000" strokeWidth={0} />
          <text
            textAnchor="middle"
            y={-20}
            fill={purple[400]}
            style={{
              fontFamily: 'system-ui',
              fontSize: '1.2em',
              fontWeight: 'bold',
            }}
          >
            {toName}
          </text>
        </Marker>
      </ComposableMap>
    </Box>
  );
};

export default MapChart;
