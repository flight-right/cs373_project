import * as React from 'react';
import {Grid, Link, Paper, Typography} from '@mui/material';
import {Flight, get_duration} from '../index';
import {ArrowRight} from '@mui/icons-material';

export default function FlightCard(props: Flight) {
  return (
    <Paper elevation={0} sx={{p: 2}} id={'results'}>
      <Grid container justifyContent={'space-between'} spacing={4}>
        <Grid item xs={2}>
          <Typography variant={'body2'}>{props.flight_number}</Typography>
          {/*<Typography variant={'body2'}>{props.model}</Typography>*/}
          <Typography variant={'body2'}>
            <Link href={`/flights/${props.id}`} underline={'none'}>
              Learn More
            </Link>
          </Typography>
        </Grid>

        <Grid item xs={7}>
          <Typography variant={'subtitle1'}>
            {props.departure_time.format('h:mma')}
            {' - '}
            {props.arrival_time.format('h:mma')}
          </Typography>
          <Typography variant={'subtitle2'} color={'text.secondary'}>
            {props.departure_airport.name}
            <ArrowRight
              fontSize={'small'}
              sx={{verticalAlign: 'text-bottom'}}
            />
            {props.arrival_airport.name}
          </Typography>
        </Grid>

        <Grid item xs={3} color={'text.secondary'}>
          <Typography variant={'body2'}>
            {props.departure_time.format('dddd, MMMM Do')}
          </Typography>
          <Typography variant={'body2'}>
            {get_duration(props.arrival_time, props.departure_time)}
          </Typography>
        </Grid>

        {/*<Grid item>*/}
        {/*  <Typography variant={'h5'}>{formatter.format(props.cost)}</Typography>*/}
        {/*</Grid>*/}
      </Grid>
    </Paper>
  );
}
