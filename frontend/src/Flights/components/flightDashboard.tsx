import React, {useEffect} from 'react';
import {Breadcrumbs, Container, Grid, Link} from '@mui/material';

import FlightsDisplay from './flightDataDisplay';
import {Flight} from '../index';
import {buildQuery, fetchAllFlightData} from '../fetchData';
import {useLocation} from 'react-router';
import {
  DataControls,
  FilterControls,
  rowOptions,
  sortOptions,
} from './flightControls';

// https://v5.reactrouter.com/web/example/query-parameters
// A custom hook that builds on useLocation to parse
// the query string for you.
function useQuery() {
  const {search} = useLocation();

  return React.useMemo(() => new URLSearchParams(search), [search]);
}

export function FlightDashboard() {
  const [totalCount, setTotalCount] = React.useState(0);
  const [rows, setRows] = React.useState<Flight[]>([]);
  const [page, setPage] = React.useState(1);
  const [limit, setLimit] = React.useState(rowOptions[0]);
  const [sortby, setsortby] = React.useState('featured');

  const [fromAirport, setFromAirport] = React.useState<string[]>([]);
  const [toAirport, setToAirport] = React.useState<string[]>([]);
  const [searchValue, setSearchValue] = React.useState('');

  function assignDefined(target, ...sources) {
    for (const source of sources) {
      for (const key of Object.keys(source)) {
        const val = source[key];
        if (val) {
          target[key] = val;
        }
      }
    }
    return target;
  }

  const getArgs = (extras?: any) => {
    const sort = sortOptions[sortby];
    return assignDefined(
      {},
      {
        limit: limit,
        page: page,
        arrival_airport: toAirport.length > 0 ? toAirport.join(',') : undefined,
        departure_airport:
          fromAirport.length > 0 ? fromAirport.join(',') : undefined,
        reverse: sort.reverse,
        search_term: searchValue,
        ...extras,
      }
    );
  };

  const buildQueryString = (args?: any) => {
    args = getArgs(args);
    const queryParams = buildQuery(args);
    // console.log('query', queryParams);
    return queryParams.toString();
  };

  const query = useQuery();

  async function runQuery(args?: any) {
    setRows([]);
    const params = buildQueryString(args);
    // console.log('run query', params);
    const data = await fetchAllFlightData(params);
    setRows(data.rows);
    setTotalCount(data.total);
  }

  async function runQueryGiven(query: URLSearchParams) {
    const data = await fetchAllFlightData(query.toString());
    setRows(data.rows);
    setTotalCount(data.total);
  }

  useEffect(() => {
    document.title = 'Upcoming Flights - Flight Right';

    query.get('page') && setPage(parseInt(query.get('page') as string));
    query.get('limit') && setLimit(parseInt(query.get('limit') as string));

    const sort_id = query.get('sort_by') || 'id';
    const reverse_val = query.get('reverse') === 'true';
    const sortOption =
      Object.keys(sortOptions).find(
        sort =>
          sortOptions[sort].id === sort_id &&
          sortOptions[sort].reverse === reverse_val
      ) || 'featured';
    setsortby(sortOption);

    query.get('departure_airport') &&
      setFromAirport((query.get('departure_airport') as string).split(','));
    query.get('arrival_airport') &&
      setToAirport((query.get('arrival_airport') as string).split(','));

    query.get('search_term') &&
      setSearchValue(query.get('search_term') as string);
    // parse query params from url and set our hooks properly
    // fetch flight data should consider query params in query

    if (Array.from(query.entries()).length > 0) {
      runQueryGiven(query);
    } else {
      runQuery();
    }
  }, []);

  return (
    <Container sx={{mt: 4, mb: 4}}>
      <Breadcrumbs aria-label="breadcrumb">
        <Link underline="hover" color="inherit" href="/">
          Home
        </Link>
        <Link underline="hover" color="inherit" href="/flights">
          Flights
        </Link>
      </Breadcrumbs>
      {/*<Typography variant={'h4'}>Upcoming Flights</Typography>*/}

      <Grid container sx={{mt: 4}} spacing={2}>
        <Grid item xs={12} sm={3}>
          <FilterControls
            // departureDate={departureDate}
            // setDepartureDate={setDepartureDate}
            // duration={duration}
            // setDuration={setDuration}
            fromAirlines={fromAirport}
            setFromAirlines={setFromAirport}
            toAirlines={toAirport}
            setToAirlines={setToAirport}
            buildQueryString={buildQueryString}
            searchValue={searchValue}
            setSearchValue={setSearchValue}
          />
        </Grid>

        <Grid item xs>
          <DataControls
            size={totalCount}
            limit={limit}
            page={page}
            sort={sortby}
            changePage={setPage}
            changeLimit={setLimit}
            changeSort={setsortby}
            buildQueryString={buildQueryString}
            runQuery={runQuery}
          />

          <FlightsDisplay rows={rows} highlight={searchValue} />

          <DataControls
            size={totalCount}
            limit={limit}
            page={page}
            changePage={setPage}
            changeLimit={setLimit}
            buildQueryString={buildQueryString}
            runQuery={runQuery}
          />
        </Grid>
      </Grid>
    </Container>
  );
}
