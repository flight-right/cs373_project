import React, {useEffect, useRef} from 'react';
import {
  Box,
  Button,
  Checkbox,
  FormControl,
  InputLabel,
  ListItemText,
  MenuItem,
  OutlinedInput,
  Pagination,
  Paper,
  Select,
  SelectChangeEvent,
  Stack,
  TextField,
  Typography,
} from '@mui/material';

import {airport_names} from '../data';
import SendIcon from '@mui/icons-material/Send';

export interface SortOption {
  name: string;
  id: string;
  reverse: boolean;
}

const useDidMountEffect = (func, deps) => {
  const didMount = useRef(false);

  useEffect(() => {
    if (didMount.current) func();
    else didMount.current = true;
  }, deps);
};

interface DataControlsProps {
  size: number;
  limit: number;
  page: number;
  sort?: string;
  changePage: (value: number) => void;
  changeLimit: (value: number) => void;
  changeSort?: (value: string) => void;
  buildQueryString: () => string;
  runQuery: (args?: any) => void;
}

export const rowOptions = [10, 25, 50];
export const sortOptions = {
  featured: {name: 'Featured', id: 'id', reverse: false},
  num: {
    name: 'Flight Number (ascending)',
    id: 'number',
    reverse: false,
  },
  num_rev: {
    name: 'Flight Number (descending)',
    id: 'number',
    reverse: true,
  },
  departure: {
    name: 'Departure Time (early first)',
    id: 'depature_time',
    reverse: false,
  },
  departure_rev: {
    name: 'Departure Time (latest first)',
    id: 'depature_time',
    reverse: true,
  },
  arrival: {
    name: 'Arrival Time (early first)',
    id: 'arrival_time',
    reverse: false,
  },
  arrival_rev: {
    name: 'Arrival Time (latest first)',
    id: 'arrival_time',
    reverse: true,
  },
};

export function DataControls({
  changePage,
  changeLimit,
  changeSort,
  page,
  limit,
  sort,
  size,
  buildQueryString,
  runQuery,
}: DataControlsProps) {
  const handlePageChange = (
    event: React.ChangeEvent<unknown>,
    value: number
  ) => {
    changePage(value);
    runQuery({page: value});
  };

  const handleChangeLimit = (event: SelectChangeEvent) => {
    const newLimit = parseInt(event.target.value);
    const newPage = 1;
    changeLimit(newLimit);
    changePage(newPage);
    runQuery({limit: newLimit, page: newPage});
  };

  const handleChangeSort = (event: SelectChangeEvent) => {
    if (changeSort) {
      const sortby = event.target.value;
      // console.log(sortby);
      changeSort(sortby);

      const sortData = sortOptions[sortby];
      // console.log(sortData);
      runQuery({sort_by: sortData.id, reverse: sortData.reverse});
    }
  };

  useDidMountEffect(() => {
    console.log('pushing history', buildQueryString());
    window.history.pushState(null, '', '?' + buildQueryString());
    // runQuery();
  }, [page, limit, sort]);

  return (
    <Stack
      direction={'row'}
      justifyContent={'space-between'}
      alignItems={'center'}
    >
      <Pagination
        count={Math.ceil(size / limit)}
        page={page}
        onChange={handlePageChange}
        shape={'rounded'}
      />
      <Stack direction={'row'} spacing={2} alignItems={'center'}>
        <Typography variant={'body1'} color={'text.secondary'}>
          {size} flights found
        </Typography>
        {sort && (
          <FormControl>
            <InputLabel id="demo-simple-select-label">Sort By</InputLabel>
            <Select
              sx={{bgcolor: 'white'}}
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={sort}
              label="Sort By"
              onChange={handleChangeSort}
            >
              {Object.keys(sortOptions).map(sort => (
                <MenuItem value={sort} key={sort}>
                  {sortOptions[sort].name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        )}
        <FormControl>
          <InputLabel id="demo-simple-select-label">Items</InputLabel>
          <Select
            sx={{bgcolor: 'white'}}
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={limit.toString()}
            label="Items"
            onChange={handleChangeLimit}
          >
            {rowOptions.map(count => (
              <MenuItem value={count} key={count}>
                {count}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Stack>
    </Stack>
  );
}

interface FlightControlsProps {
  fromAirlines: Array<string>;
  setFromAirlines: (value: Array<string>) => void;
  toAirlines: Array<string>;
  setToAirlines: (value: Array<string>) => void;
  searchValue: string;
  setSearchValue: (value: string) => void;

  buildQueryString: () => string;
}

function AirportSelector(props: {
  label: string;
  value: any[];
  onChange: (event) => void;
}) {
  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 8;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };
  return (
    <Box>
      <InputLabel id="demo-multiple-checkbox-label">{props.label}</InputLabel>
      <Select
        labelId="demo-multiple-checkbox-label"
        id="demo-multiple-checkbox"
        multiple
        value={props.value}
        onChange={props.onChange}
        input={<OutlinedInput label="Tag" />}
        renderValue={selected => selected.join(', ')}
        MenuProps={MenuProps}
        fullWidth
      >
        {airport_names.map(name => (
          <MenuItem key={name} value={name}>
            <Checkbox checked={props.value.indexOf(name) > -1} />
            <ListItemText primary={name} />
          </MenuItem>
        ))}
      </Select>
    </Box>
  );
}

export function FilterControls(props: FlightControlsProps) {
  const createAirlineHandler = setHook => {
    return event => {
      const {
        target: {value},
      } = event;
      setHook(
        // On autofill we get a the stringified value.
        typeof value === 'string' ? value.split(',') : value
      );
    };
  };

  const handleSearchChange = event => {
    props.setSearchValue(event.target.value);
  };

  return (
    <Box>
      <Typography variant={'h4'} py={1} gutterBottom>
        Filter By
      </Typography>

      <Paper elevation={0} sx={{p: 2, mt: 4}}>
        <Stack direction={'column'} spacing={4}>
          <AirportSelector
            label={'Departing From'}
            value={props.fromAirlines}
            onChange={createAirlineHandler(props.setFromAirlines)}
          />

          <AirportSelector
            label={'Arriving At'}
            value={props.toAirlines}
            onChange={createAirlineHandler(props.setToAirlines)}
          />

          <TextField
            // required
            id="outlined-required"
            label="Search Term"
            value={props.searchValue}
            onChange={handleSearchChange}
            // defaultValue="Hello World"
          />

          <Button
            variant={'contained'}
            size={'large'}
            endIcon={<SendIcon />}
            fullWidth
            href={'?' + props.buildQueryString()}
            disableElevation
          >
            Search
          </Button>
        </Stack>
      </Paper>
    </Box>
  );
}
