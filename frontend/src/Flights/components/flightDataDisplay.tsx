// import 'https://unpkg.com/hrjs';
import * as React from 'react';
import FlightCard from './flightCard';
import {Skeleton, Stack} from '@mui/material';
import {Flight} from '../index';
import {useLayoutEffect} from 'react';

import HR from '../hr';

interface FlightsTableProps {
  rows: Flight[];
  highlight: string;
}

export default function FlightsTable(props: FlightsTableProps) {
  // console.log(props.rows);

  useLayoutEffect(() => {
    console.log('highlighting', props.highlight);
    if (props.highlight) {
      new HR('#results', {
        highlight: props.highlight,
        // replaceWith: props.highlight,
        backgroundColor: '#B4FFEB',
      }).hr();
    }
  }, [props.rows]);

  return (
    <Stack spacing={2} sx={{my: 4}}>
      {props.rows.length > 0
        ? props.rows.map((row, index) => <FlightCard key={index} {...row} />)
        : Array.from({length: 10}, (_, i) => (
            <Skeleton
              variant="rectangular"
              width={'100%'}
              height={118}
              key={i}
            />
          ))}
    </Stack>
  );
}
