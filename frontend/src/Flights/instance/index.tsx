import React, {useEffect, useState} from 'react';
import {useParams} from 'react-router';
import {
  Box,
  Breadcrumbs,
  CircularProgress,
  Container,
  Link,
  Stack,
  Typography,
  Chip,
  Card,
  CardContent,
  Grid,
} from '@mui/material';
import {convertAPItoData, fetchFlightData} from '../fetchData';
import {ArrowRight} from '@mui/icons-material';
import moment from 'moment';
import {Airport, Flight, get_duration} from '../index';
import {green, purple, red} from '@mui/material/colors';
import SimpleMap from '../components/map';

import Avatar from '@mui/material/Avatar';

interface ParamTypes {
  id: string;
}

const statusMap = {
  'On Time': green[500],
  late: red[500],
  scheduled: purple[500],
  black: 'black',
};

const StatusChip = (props: React.PropsWithChildren<{type: string}>) => {
  return (
    <Chip
      size={'small'}
      sx={{
        mb: 1,
        borderRadius: 2,
        // eslint-disable-next-line
        background: (statusMap as any)[props.type] || 'black',
        color: 'white',
      }}
      label={props.children}
    />
  );
};

function TerminalInfo(props: {airport: Airport; time: moment.Moment}) {
  return (
    <Card variant={'outlined'} sx={{flexGrow: 1}}>
      <CardContent>
        <Typography variant={'subtitle1'}>
          <Link href={`/airports/${props.airport.id}`}>
            {props.airport.name}
          </Link>
          {'  '}
          <StatusChip type={'black'}>{props.airport.code}</StatusChip>
        </Typography>
        <Typography variant={'body2'} color={'text.secondary'}>
          {props.time.format('HH:mma dddd, MMMM Do')}
        </Typography>
      </CardContent>
    </Card>
  );
}

const FlightInstance = () => {
  const {id} = useParams<ParamTypes>();

  const [data, setData] = useState<Flight>();

  useEffect(() => {
    // document.title = 'Flight - Flight Right';
    const fetchData = async () => {
      const data = await fetchFlightData(id);
      setData(convertAPItoData(data));
      // console.log(data);
      document.title = `Flight ${data.flight_iata} - Flight Right`;
    };

    fetchData();
  }, []);

  return (
    <Container>
      <Breadcrumbs aria-label="breadcrumb">
        <Link underline="hover" color="inherit" href="/">
          Home
        </Link>
        <Link underline="hover" color="inherit" href="/flights">
          Flights
        </Link>
        {data && (
          <Typography color="text.primary">
            Flight {data.flight_number}
          </Typography>
        )}
      </Breadcrumbs>

      {!data && <CircularProgress />}
      {data && (
        <Grid
          container
          sx={{mt: 4, mb: 4}}
          columns={6}
          spacing={2}
          justifyContent={'center'}
        >
          <Grid item xs={12}>
            <Stack
              direction={'row'}
              justifyContent={'space-between'}
              alignItems={'start'}
            >
              <Box>
                <Typography variant={'h4'}>
                  Flight {data.flight_number}
                  {'  '}
                  <StatusChip type={data.status}>{data.status}</StatusChip>
                </Typography>
                <Typography variant={'subtitle1'} gutterBottom>
                  <Link href={`/airlines/${data.airline.id}`}>
                    {data.airline.airline_name} ({data.airline.airline_iata})
                    <Avatar
                      alt="Airline Logo"
                      src={data.airline.airline_logo}
                      sx={{
                        width: 100,
                        height: 100,
                        display: 'flex',
                        flexDirection: 'row',
                      }}
                    />
                  </Link>

                  {/*{'  '}*/}
                  {/*{data.aircraft_model}*/}
                </Typography>
              </Box>
              {/*<Box>*/}
              {/*  <Typography variant={'h2'} component={'h5'}>*/}
              {/*    ${data.cost}*/}
              {/*  </Typography>*/}
              {/*</Box>*/}
            </Stack>
          </Grid>
          <Grid item xs={12} md={6}>
            <Box>
              <Grid container spacing={2} alignItems={'center'}>
                <Grid item xs>
                  <TerminalInfo
                    airport={data.departure_airport}
                    time={data.departure_time}
                  />
                </Grid>
                <Grid item>
                  <Box>
                    {/*{https://stackoverflow.com/questions/33684748/get-timezone-difference-in-hours-with-moment}*/}
                    <ArrowRight fontSize={'large'} />
                    <Typography variant={'body2'}>
                      {get_duration(
                        moment(data.arrival_time),
                        moment(data.departure_time)
                      )}
                    </Typography>
                  </Box>
                </Grid>
                <Grid item xs>
                  <TerminalInfo
                    airport={data.arrival_airport}
                    time={data.arrival_time}
                  />
                </Grid>
              </Grid>
            </Box>
          </Grid>

          <Grid item xs={12} md={4}>
            <SimpleMap
              from={data.departure_airport}
              to={data.arrival_airport}
            />
          </Grid>

          {/*<Grid item xs={12} md={4}></Grid>*/}
        </Grid>
      )}
    </Container>
  );
};

export default FlightInstance;
