import axios from 'axios';
import {FlightAPI, Flight, AllAPIData} from './index';
import moment from 'moment';

export function buildQuery(args: any) {
  return new URLSearchParams(args).toString();
}

export function convertAPItoData(apiData: FlightAPI): Flight {
  const departure_time = moment(apiData.flight_dept_time);
  const arrival_time = moment(apiData.flight_arr_time);
  const duration = arrival_time.diff(departure_time, 'minutes');

  return {
    ...apiData,
    arrival_airport: {
      id: apiData.airport ? apiData.airport.id : 0,
      name: apiData.flight_arr_airport,
      code: apiData.flight_arr_airport_iata,
      lon: 0.0,
      lat: 0.0,
    },
    arrival_time,
    departure_airport: {
      id: apiData.airport ? apiData.airport.id : 0,
      name: apiData.flight_dept_airport,
      code: apiData.flight_dept_airport_iata,
      lon: apiData.airport ? apiData.airport.airport_long : 0.0,
      lat: apiData.airport ? apiData.airport.airport_lat : 0.0,
    },
    airline: {
      airline_name: apiData.flight_airline_name,
      ...apiData.airline,
    },
    departure_time,
    duration,
    status: apiData.flight_status,
    flight_number: apiData.flight_iata,
  };
}

export function fetchFlightData(flightId: string) {
  const host = 'https://api.flightright.me';
  return axios
    .get<FlightAPI>(host + '/flights/' + flightId)
    .then(data => data.data)
    .then(data => {
      return data;
    });
}

export function fetchAllFlightData(params = {}) {
  const host = 'https://api.flightright.me';
  return axios
    .get<AllAPIData<FlightAPI>>(host + '/flights?' + buildQuery(params))
    .then(data => data.data)
    .then(data => {
      return {
        total: data.total,
        rows: data.data.map(item => convertAPItoData(item)),
      };
    });
}
