import React from 'react';
import moment from 'moment';
import {Route, Switch, useRouteMatch} from 'react-router-dom';
import {FlightDashboard} from './components/flightDashboard';
import FlightsInstance from './instance/index';
// import {ThemeProvider} from '@mui/styles';
import {createTheme, ThemeProvider} from '@mui/material';
import {purple} from '@mui/material/colors';

export interface Flight {
  id: number;
  flight_number: string;
  // model: string;
  // cost: number;
  departure_time: moment.Moment;
  arrival_time: moment.Moment;
  departure_airport: Airport;
  arrival_airport: Airport;
  airline: Airline;
  duration: number;
  status: string;
}

export interface Airline {
  airline_name: string;
  airline_country?: string;
  airline_av_fleet?: number;
  airline_fleet_size?: number;
  airline_founded?: number;
  airline_hub_code?: string;
  airline_iata?: string;
  airline_icao?: string;
  airline_logo?: string;
  id?: number;
}

export interface Airport {
  id: number;
  name: string;
  code: string;
  lat: number;
  lon: number;
}

export interface AirlineAPI {
  airline_av_fleet: number;
  airline_country: string;
  airline_fleet_size: number;
  airline_founded: number;
  airline_hub_code: string;
  airline_iata: string;
  airline_icao: string;
  airline_name: string;
  airline_logo: string;
  airline_news_img: string; // img link
  airline_news_text: string;
  airports: AirportAPI[];
  flights: FlightAPI[];
  id: number;
}

export interface AirportAPI {
  airlines: AirlineAPI[];
  airport_address: string;
  airport_city: string;
  airport_flight_volume: 0;
  airport_iata: string;
  airport_icao: string;
  airport_lat: number;
  airport_long: number;
  airport_maps: string;
  airport_name: string;
  airport_phone_number: string;
  airport_rating: number;
  airport_runway_number: number;
  airport_state: string;
  airport_timezone: string;
  airport_twitter: string;
  airport_website: string;
  flights: FlightAPI[];
  id: number;
}

export interface FlightAPI {
  airline?: AirlineAPI;
  airport?: AirportAPI;
  flight_aircraft_img: string; // url?
  flight_airline_name: string;
  flight_arr_airport: string;
  flight_arr_airport_iata: string;
  flight_arr_airport_icao: string;
  flight_arr_time: string;
  flight_dept_airport: string;
  flight_dept_airport_iata: string;
  flight_dept_airport_icao: string;
  flight_dept_time: string;
  flight_iata: string;
  flight_icao: string;
  flight_lat: number;
  flight_long: number;
  flight_number: string;
  flight_status: string;
  id: number;
}

export interface AllAPIData<Type> {
  count: number;
  total: number;
  data: Type[];
}

export function get_duration(
  arrival_time: moment.Moment,
  departure_time: moment.Moment
) {
  const hours = arrival_time.diff(departure_time, 'hours');
  const minutes = arrival_time.diff(departure_time, 'minutes') % 60;
  return hours > 0 && minutes > 0
    ? `${hours}h ${minutes}m`
    : hours > 0
    ? `${hours}h`
    : `${minutes}m`;
}

const theme = createTheme({
  palette: {
    primary: {
      main: purple[400],
    },
  },
});

export default function Flights() {
  const match = useRouteMatch();
  return (
    <ThemeProvider theme={theme}>
      <Switch>
        <Route path={`${match.path}/:id`}>
          <FlightsInstance />
        </Route>

        <Route path={match.path}>
          <FlightDashboard />
        </Route>
      </Switch>
    </ThemeProvider>
  );
}
