import React from 'react';
import Airports from '../index';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route} from 'react-router-dom';
import AirportInstance from '../components/AirportInstance';

it('loads model page(s) without issue', () => {
  const div = document.createElement('div');

  ReactDOM.render(
    <BrowserRouter>
      <Route exact path="/airports/limit=9&page=1" component={Airports} />
    </BrowserRouter>,
    div
  );
  expect(div.textContent).not.toBe(null);
});

it('loads instance pages without issue', () => {
  const div = document.createElement('div');

  ReactDOM.render(
    <BrowserRouter>
      <Route exact path="/airports/:id" component={AirportInstance} />
    </BrowserRouter>,
    div
  );
  expect(div.textContent).not.toBe(null);
});

it('loads model page cards for sorting without issue', () => {
  const div = document.createElement('div');

  ReactDOM.render(
    <BrowserRouter>
      <Route
        exact
        path="/airports/limit=9&page=1&sort_by=name"
        component={Airports}
      />
    </BrowserRouter>,
    div
  );
  expect(div.textContent).not.toBe(null);
});

it('loads model page cards for filtering without issue', () => {
  const div = document.createElement('div');

  ReactDOM.render(
    <BrowserRouter>
      <Route
        exact
        path="/airports/limit=9&page=1&state=florida"
        component={Airports}
      />
    </BrowserRouter>,
    div
  );
  expect(div.textContent).not.toBe(null);
});

it('loads model page cards for filtering AND sorting without issue', () => {
  const div = document.createElement('div');

  ReactDOM.render(
    <BrowserRouter>
      <Route
        exact
        path="/airports/limit=9&page=1&sort_by=name&state=florida"
        component={Airports}
      />
    </BrowserRouter>,
    div
  );
  expect(div.textContent).not.toBe(null);
});
