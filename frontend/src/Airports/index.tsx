import React, {useEffect, useState} from 'react';
import {Container, Grid} from '@mui/material';
import axios from 'axios';
import AirportModelCard from './components/AirportModelCard';
import ModelPagination from '../components/ModelPagination';
import {
  Breadcrumbs,
  LinearProgress,
  Select,
  MenuItem,
  InputLabel,
  FormControl,
  Typography,
  TextField,
  Button,
  Link,
} from '@mui/material';
import {RouteComponentProps, useHistory} from 'react-router-dom';
import {cities, states} from './components/data/filterLists';

export interface AirportAPI {
  count: number;
  data: Airport[];
  total: number;
}

export interface Airport {
  airport_timezone: number;
  airport_name: string;
  airport_flight_volume: number;
  airport_iata: string;
  airport_icao: string;
  airport_lat: number;
  airport_long: number;
  airport_rating: number;
  airport_website: string;
  airport_runway_number: number;
  airport_state: string;
  airport_city: string;
  airport_maps: string;
  id: number;
}

const defaultAirlines: Airport[] = [];

const Airports = ({
  match,
}: RouteComponentProps<{
  limit: string;
  page: string;
  sort: string;
  filter: string;
  search: string;
}>) => {
  const [airports, setAirports]: [Airport[], (airports: Airport[]) => void] =
    useState(defaultAirlines);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(
    match.params.page.includes('&sort_by=') ||
      match.params.page.includes('&state=')
      ? Number(match.params.page.substring(0, match.params.page.indexOf('&')))
      : Number(match.params.page)
  );
  const [itemsPerPage] = useState(Number(match.params.limit));
  const [airportsFound, setAirportsFound] = useState(0);
  const [sort] = useState(
    match.params.page.includes('&sort_by=')
      ? match.params.page.includes('&state=')
        ? match.params.page.substring(
            match.params.page.indexOf('&sort_by='),
            match.params.page.indexOf('&state=')
          )
        : match.params.page.substring(match.params.page.indexOf('&sort_by='))
      : ''
  );

  const [search, setSearch] = useState(
    match.params.page.includes('&search_term=')
      ? match.params.page.substring(match.params.page.indexOf('&search_term='))
      : ''
  );

  const [stateFilter] = useState(
    match.params.page.includes('&state=')
      ? match.params.page.substring(match.params.page.indexOf('&state='))
      : ''
  );

  const [cityFilter] = useState(
    match.params.page.includes('&city=')
      ? match.params.page.substring(match.params.page.indexOf('&city='))
      : ''
  );

  const history = useHistory();
  const toHighlight = search === '' || search === ' ' ? false : true;
  const currentLink = window.location.href.substring(
    window.location.href.lastIndexOf('/') + 1
  );

  useEffect(() => {
    fetchAirports(
      match.params.limit,
      match.params.page,
      sort,
      stateFilter,
      cityFilter,
      search
    );
  }, []);

  const fetchAirports = (
    limit: string,
    page: string,
    sort: string,
    stateFilter: string,
    cityFilter: string,
    search: string
  ) => {
    setLoading(true);
    axios
      .get<AirportAPI>(
        `https://api.flightright.me/airports?limit=${limit}&page=${page}${sort}${stateFilter}${cityFilter}${search}`,
        {
          headers: {
            'Content-Type': 'application/json',
          },
        }
      )
      .then(res => {
        console.log(res);
        console.log('CURRENT PAGE BEFORE CHANGE:' + currentPage);
        console.log('SORTING BY:' + sort);
        console.log('FILTERING BY:' + stateFilter);
        console.log('SEARCH BY:' + search);
        setAirports(res.data.data);
        setLoading(false);
      })
      .catch(err => {
        console.log(err);
      });

    axios
      .get<AirportAPI>(
        `https://api.flightright.me/airports?${sort}${stateFilter}${search}`,
        {
          headers: {
            'Content-Type': 'application/json',
          },
        }
      )
      .then(res => {
        setAirportsFound(res.data.count);
        setLoading(false);
      });
  };

  // Change page
  const handleChange = (
    event: React.ChangeEvent<unknown>,
    pageNumber: number
  ) => {
    history.push(
      currentLink.replace('&page=' + currentPage, '&page=' + pageNumber)
    );

    fetchAirports(
      '' + itemsPerPage,
      '' + pageNumber,
      sort,
      stateFilter,
      cityFilter,
      search
    );
    setCurrentPage(pageNumber);
  };

  // Handles the searching for model page
  const handleSearch = () => {
    const searchTerms =
      document.getElementById('search-box') === null
        ? ''
        : (document.getElementById('search-box')! as HTMLInputElement).value;

    setSearch(
      (document.getElementById('search-box')! as HTMLInputElement).value
    );

    history.push(
      currentLink.substring(0, currentLink.indexOf('&page=' + currentPage)) +
        '&page=1&search_term=' +
        searchTerms
    );

    fetchAirports(
      '' + itemsPerPage,
      '' + 1,
      sort,
      stateFilter,
      cityFilter,
      '&search_term=' + searchTerms
    );
    setCurrentPage(1);
  };

  return (
    <React.Fragment>
      {loading ? (
        <Container>
          <LinearProgress />
        </Container>
      ) : (
        <Container>
          <Grid container>
            <div role="presentation">
              <Breadcrumbs aria-label="breadcrumb">
                <Link underline="hover" color="inherit" href="/">
                  Home
                </Link>
                <Link
                  underline="hover"
                  color="inherit"
                  href={'/airports/limit=' + itemsPerPage + '&page=1'}
                >
                  Airports
                </Link>
              </Breadcrumbs>
            </div>
          </Grid>

          <Grid container spacing="15" sx={{marginTop: 2, marginBottom: 2}}>
            <Grid item justify-items="start">
              <TextField
                id="search-box"
                label="Search"
                sx={{height: 40, minWidth: 250}}
                // onSubmit={handleSearch}
              ></TextField>
              <Button
                variant="contained"
                sx={{marginLeft: 1, height: 55}}
                onClick={handleSearch}
              >
                Search
              </Button>
            </Grid>

            <Grid item sx={{minWidth: 140}} justify-items="end">
              <FormControl fullWidth>
                <InputLabel id="sort-label">Sort By</InputLabel>
                <Select labelId="sort-label" label="Sort By">
                  <MenuItem
                    component={Link}
                    href={
                      currentLink.includes('&sort_by=')
                        ? currentLink.replace(sort, '')
                        : currentLink
                    }
                  >
                    None
                  </MenuItem>
                  <MenuItem
                    component={Link}
                    href={
                      currentLink.includes('&sort_by=')
                        ? currentLink.replace(sort, '&sort_by=name')
                        : currentLink + '&sort_by=name'
                    }
                  >
                    Name (asc.)
                  </MenuItem>
                  <MenuItem
                    component={Link}
                    href={
                      currentLink.includes('&sort_by=')
                        ? currentLink.replace(sort, '&sort_by=name&reverse')
                        : currentLink + '&sort_by=name&reverse'
                    }
                  >
                    Name (desc.)
                  </MenuItem>

                  <MenuItem
                    component={Link}
                    href={
                      currentLink.includes('&sort_by=')
                        ? currentLink.replace(sort, '&sort_by=city')
                        : currentLink + '&sort_by=city'
                    }
                  >
                    City (asc.)
                  </MenuItem>
                  <MenuItem
                    component={Link}
                    href={
                      currentLink.includes('&sort_by=')
                        ? currentLink.replace(sort, '&sort_by=city&reverse')
                        : currentLink + '&sort_by=city&reverse'
                    }
                  >
                    City (desc.)
                  </MenuItem>
                  <MenuItem
                    component={Link}
                    href={
                      currentLink.includes('&sort_by=')
                        ? currentLink.replace(sort, '&sort_by=state')
                        : currentLink + '&sort_by=state'
                    }
                  >
                    State (asc.)
                  </MenuItem>
                  <MenuItem
                    component={Link}
                    href={
                      currentLink.includes('&sort_by=')
                        ? currentLink.replace(sort, '&sort_by=state&reverse')
                        : currentLink + '&sort_by=state&reverse'
                    }
                  >
                    State (desc.)
                  </MenuItem>
                  <MenuItem
                    component={Link}
                    href={
                      currentLink.includes('&sort_by=')
                        ? currentLink.replace(sort, '&sort_by=flight_volume')
                        : currentLink + '&sort_by=flight_volume'
                    }
                  >
                    Flight Volume (asc.)
                  </MenuItem>
                  <MenuItem
                    component={Link}
                    href={
                      currentLink.includes('&sort_by=')
                        ? currentLink.replace(
                            sort,
                            '&sort_by=flight_volume&reverse'
                          )
                        : currentLink + '&sort_by=flight_volume&reverse'
                    }
                  >
                    Flight Volume (desc.)
                  </MenuItem>
                </Select>
              </FormControl>
            </Grid>

            <Grid item sx={{minWidth: 140}} justify-items="end">
              <FormControl fullWidth>
                <InputLabel id="filter-label">State</InputLabel>
                <Select labelId="filter-label" label="state">
                  <MenuItem
                    component={Link}
                    href={
                      currentLink.includes('&state=')
                        ? currentLink.replace(stateFilter, '')
                        : currentLink
                    }
                  >
                    All
                  </MenuItem>
                  {states.map(state => (
                    <MenuItem
                      key={state}
                      component={Link}
                      href={
                        currentLink.includes(`${state},`)
                          ? currentLink.replace(`${state},`, '')
                          : currentLink.includes('&state= ')
                          ? currentLink
                          : currentLink.includes('&state=')
                          ? currentLink + `${state},`
                          : currentLink + `&state=${state},`
                      }
                    >
                      {state}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>

            <Grid item sx={{minWidth: 140}} justify-items="end">
              <FormControl fullWidth>
                <InputLabel id="filter-label">City</InputLabel>
                <Select labelId="filter-label" label="city">
                  <MenuItem
                    component={Link}
                    href={
                      currentLink.includes('&city=')
                        ? currentLink.replace(cityFilter, '')
                        : currentLink
                    }
                  >
                    All
                  </MenuItem>
                  {cities.map(city => (
                    <MenuItem
                      key={city}
                      component={Link}
                      href={
                        currentLink.includes(`${city},`)
                          ? currentLink.replace(`${city},`, '')
                          : currentLink.includes('&city= ')
                          ? currentLink
                          : currentLink.includes('&city=')
                          ? currentLink + `${city},`
                          : currentLink + `&city=${city},`
                      }
                    >
                      {city}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
          </Grid>

          <Grid container paddingBottom={2} justifyContent="left">
            <Grid item>
              <ModelPagination
                itemsPerPage={itemsPerPage}
                totalItems={airportsFound}
                handleChange={handleChange}
                page={currentPage}
              />
            </Grid>
          </Grid>

          <Grid container spacing={'15'}>
            {airports.map(airport => (
              <Grid item xs={3} key={airport.airport_name}>
                <AirportModelCard
                  airport_photo={airport.airport_maps}
                  airport_name={airport.airport_name}
                  airport_flight_volume={airport.airport_flight_volume}
                  airport_runway_number={airport.airport_runway_number}
                  airport_iata={airport.airport_iata}
                  airport_icao={airport.airport_icao}
                  airport_id={airport.id}
                  toHighlight={toHighlight}
                  searchTerm={search}
                  airport_state={airport.airport_state}
                  airport_city={airport.airport_city}
                />
              </Grid>
            ))}
          </Grid>
          <Grid container paddingTop={2} justifyContent="left">
            <Typography variant="inherit">
              {airportsFound} airports found
            </Typography>
          </Grid>
        </Container>
      )}
    </React.Fragment>
  );
};

export default Airports;
