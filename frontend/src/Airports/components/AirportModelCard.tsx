import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
// import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import {Button, CardActions} from '@mui/material';
import {Link} from 'react-router-dom';
import Highlighter from 'react-highlight-words';

type CardInfo = {
  airport_name: string;
  airport_flight_volume: number;
  airport_iata: string;
  airport_icao: string;
  airport_runway_number: number;
  airport_photo: string;
  airport_id: number;
  airport_state: string;
  airport_city: string;
  toHighlight: boolean;
  searchTerm: string;
};

export default function AirportModelCard(props: CardInfo) {
  let picture: string = props.airport_photo;
  console.log('picture2: ' + picture);

  // Check if a logo exists or not, return logo not found if not.
  if (picture === 'null') {
    picture =
      'https://user-images.githubusercontent.com/24848110/33519396-7e56363c-d79d-11e7-969b-09782f5ccbab.png';
  }

  console.log('picture2: ' + picture);

  const search = props.searchTerm.includes('&search_term=')
    ? props.searchTerm.replace('&search_term=', '')
    : props.searchTerm;
  console.log(search);

  return props.toHighlight ? (
    <Card sx={{maxWidth: 500}}>
      {/* <CardMedia
        sx={{objectFit: 'none'}}
        component="img"
        height="100"
        image={picture}
        alt="AIRPORT_IMAGE"
      /> */}
      <CardContent>
        <Typography variant="h6" component="div">
          <Highlighter
            searchWords={[search]}
            textToHighlight={props.airport_name}
          />
        </Typography>

        <Typography variant="body1" color="text.primary">
          <Highlighter
            searchWords={[search]}
            textToHighlight={'State: ' + props.airport_state}
          />
        </Typography>

        <Typography variant="body1" color="text.primary">
          <Highlighter
            searchWords={[search]}
            textToHighlight={'City: ' + props.airport_city}
          />
        </Typography>

        {/* <Typography variant="body1" color="text.primary">
          <Highlighter
            searchWords={[search]}
            textToHighlight={'Flight Volume: ' + props.airport_flight_volume}
          />
        </Typography> */}

        <Typography variant="body1" color="text.primary">
          <Highlighter
            searchWords={[search]}
            textToHighlight={
              'Number of Runways: ' + props.airport_runway_number
            }
          />
        </Typography>

        <Typography variant="body1" color="text.primary">
          <Highlighter
            searchWords={[search]}
            textToHighlight={'IATA: ' + props.airport_iata}
          />
        </Typography>

        <Typography variant="body1" color="text.primary">
          <Highlighter
            searchWords={[search]}
            textToHighlight={'ICAO: ' + props.airport_icao}
          />
        </Typography>
      </CardContent>
      <CardActions>
        <Button
          size="large"
          color="primary"
          component={Link}
          to={`/airports/view/${props.airport_id}`}
        >
          View
        </Button>
      </CardActions>
    </Card>
  ) : (
    <Card sx={{maxWidth: 500}}>
      {/* <CardMedia
        sx={{objectFit: 'none'}}
        component="img"
        height="100"
        image={picture}
        alt="AIRPORT_IMAGE"
      /> */}
      <CardContent>
        <Typography variant="h6" component="div">
          {props.airport_name}
        </Typography>
        <Typography variant="body1" color="text.primary">
          State: {props.airport_state}
        </Typography>
        <Typography variant="body1" color="text.primary">
          City: {props.airport_city}
        </Typography>
        {/* <Typography variant="body1" color="text.primary">
          Flight Volume: {props.airport_flight_volume}
        </Typography> */}
        <Typography variant="body1" color="text.primary">
          Number of Runways: {props.airport_runway_number}
        </Typography>
        <Typography variant="body1" color="text.primary">
          IATA: {props.airport_iata}
        </Typography>
        <Typography variant="body1" color="text.primary">
          ICAO: {props.airport_icao}
        </Typography>
      </CardContent>
      <CardActions>
        <Button
          size="large"
          color="primary"
          component={Link}
          to={`/airports/view/${props.airport_id}`}
        >
          View
        </Button>
      </CardActions>
    </Card>
  );
}
