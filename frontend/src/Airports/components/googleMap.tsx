import React from 'react';
import {compose, withProps} from 'recompose';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
} from 'react-google-maps';

interface mapInfo {
  isMarkerShown: boolean;
  lat: number;
  long: number;
}

<script
  async
  defer
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBRn7xf9Z-yHsLNeeVRY2PhX0_l8NQenBM&callback=initMap"
></script>;

export const MyMapComponent = compose(
  withProps({
    googleMapURL:
      'https://maps.googleapis.com/maps/api/js?key=AIzaSyBRn7xf9Z-yHsLNeeVRY2PhX0_l8NQenBM&libraries=geometry,drawing,places',
    loadingElement: <div style={{height: '100%'}} />,
    containerElement: <div style={{height: '400px'}} />,
    mapElement: <div style={{height: '100%'}} />,
  }),
  withScriptjs,
  withGoogleMap
)((props: mapInfo) => (
  <GoogleMap defaultZoom={13} defaultCenter={{lat: props.lat, lng: props.long}}>
    {props.isMarkerShown && (
      <Marker position={{lat: props.lat, lng: props.long}} />
    )}
  </GoogleMap>
));

export default MyMapComponent;
