import * as React from 'react';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';

interface CardInfo {
  height: number;
  width: number;
  cardTitle: string;
  airport_iata: string;
  airport_icao: string;
  airport_flight_volume: number;
  airport_runway_number: number;
  airport_website: string;
  airport_address: string;
  airport_city: string;
  airport_timezone: string;
}

export default function AirportTextCard(props: CardInfo) {
  return (
    <CardContent>
      <Typography variant="h5" component="div" paddingBottom="5">
        General Information:
      </Typography>
      <Divider variant={'fullWidth'} sx={{marginTop: 1, marginBottom: 1}} />
      <Typography variant="h6">
        {'IATA: ' + props.airport_iata}
        <br />
        {'ICAO: ' + props.airport_icao}
        <br />
        {'Average Annual Flight Volume: ' + props.airport_flight_volume}
        <br />
        {'Number of Runways: ' + props.airport_runway_number}
        <br />
      </Typography>
      <br />
      <Typography variant="h5" component="div" paddingBottom="5">
        Contact Information:
      </Typography>
      <Divider variant={'fullWidth'} sx={{marginTop: 1, marginBottom: 1}} />
      <Typography variant="h6">
        {'Website: ' + props.airport_website}
        <br />
        {'Address: ' + props.airport_address}
        <br />
        {'City: ' + props.airport_city}
        <br />
        {'Timezone: GMT' + props.airport_timezone}
      </Typography>
    </CardContent>
  );
}
