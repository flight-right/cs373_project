import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {Container, Grid, Typography, Divider, Card} from '@mui/material';
import {RouteComponentProps} from 'react-router-dom';
import {Button} from '@mui/material';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import AirportTextCard from './AirportTextCard';
import CircularProgress from '@mui/material/CircularProgress';
import DataTable from '../../Airlines/components/flightTable';
import AirlineTable from './airlineTable';
import {Timeline} from 'react-twitter-widgets';
import Skeleton from '@mui/material/Skeleton';
import {useHistory} from 'react-router-dom';
import MyMapComponent from './googleMap';

interface Airport {
  airport_address: string;
  airport_city: string;
  airport_flight_volume: number;
  airport_iata: string;
  airport_icao: string;
  airport_lat: number;
  airport_long: number;
  airport_maps: string;
  airport_name: string;
  airport_phone_number: string;
  airport_runway_number: number;
  airport_state: string;
  airport_timezone: string;
  airport_twitter: string;
  airport_website: string;
  airlines: Airline[];
  flights: Flight[];
  id: number;
}

export interface Flight {
  flight_aircraft_img: string;
  flight_airline_name: string;
  flight_arr_airport: string;
  flight_arr_airport_iata: string;
  flight_arr_airport_icao: string;
  flight_arr_time: string;
  flight_dept_airport: string;
  flight_dept_airport_iata: string;
  flight_dept_airport_icao: string;
  flight_dept_time: string;
  flight_iata: string;
  flight_icao: string;
  flight_lat: number;
  flight_long: number;
  flight_number: string;
  flight_status: string;
  id: number;
}

export interface Airline {
  airline_name: string;
  airline_country: string;
  airline_logo: string;
  airline_av_fleet: number;
  airline_fleet_size: number;
  airline_founded: number;
  airline_hub_code: string;
  airline_iata: string;
  airline_icao: string;
  airline_news_img: string;
  airline_news_text: string;
  flights: Flight[];
  id: number;
}

const AirportInstance = ({match}: RouteComponentProps<{id?: string}>) => {
  const [airport, setAirport] = useState<Airport>();
  const history = useHistory();

  useEffect(() => {
    fetchAirport();
  }, []);

  const fetchAirport = () => {
    axios
      .get<Airport>(`https://api.flightright.me/airports/${match.params.id}`, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .then(res => {
        console.log(res);
        setAirport(res.data);
      })
      .catch(err => {
        console.log(err);
      });
  };

  return (
    <React.Fragment>
      <Container>
        {airport ? (
          <Button onClick={() => history.goBack()}>
            <ArrowBackIcon /> Back to Airports
          </Button>
        ) : (
          <Skeleton variant="rectangular" />
        )}
      </Container>

      <Container>
        {airport ? (
          <React.Fragment>
            <Typography
              variant={'h3'}
              // key={airline.id}
              sx={{paddingBottom: 1}}
              align="center"
            >
              {airport.airport_name} Airport
            </Typography>
            <Divider variant={'fullWidth'} />

            <Grid container>
              {/* {currentCards.map(airport => {
              return ( */}
              <Grid
                item
                textAlign="center"
                alignSelf="center"
                marginLeft="auto"
                marginRight="auto"
              >
                <Card sx={{marginTop: 2, marginBottom: 2}}>
                  <AirportTextCard
                    width={897}
                    height={500}
                    cardTitle={airport.airport_name}
                    airport_iata={airport.airport_iata}
                    airport_icao={airport.airport_icao}
                    airport_flight_volume={airport.airport_flight_volume}
                    airport_runway_number={airport.airport_runway_number}
                    airport_website={airport.airport_website}
                    airport_address={airport.airport_address}
                    airport_city={airport.airport_city}
                    airport_timezone={airport.airport_timezone}
                  />
                </Card>
              </Grid>
              <Grid
                item
                alignSelf="center"
                marginLeft="auto"
                marginRight="auto"
              >
                <AirlineTable rows={airport.airlines} />
              </Grid>

              <Grid
                item
                alignSelf="center"
                marginLeft="auto"
                marginRight="auto"
              >
                <Timeline
                  dataSource={{
                    sourceType: 'profile',
                    screenName:
                      airport.airport_twitter === null
                        ? 'iah'
                        : airport.airport_twitter,
                  }}
                  options={{
                    height: '410',
                    width: '300',
                  }}
                />{' '}
              </Grid>
            </Grid>

            <Grid container paddingBottom="2">
              <Grid
                item
                sx={{width: '93.5%', paddingBottom: 3}}
                alignSelf="center"
                marginLeft="auto"
                marginRight="auto"
              >
                <DataTable rows={airport.flights} />
              </Grid>
              <Grid item width="50%" marginLeft="auto" marginRight="auto">
                <MyMapComponent
                  isMarkerShown={true}
                  lat={airport.airport_lat}
                  long={airport.airport_long}
                />
              </Grid>
            </Grid>

            <Grid item sx={{width: '100%', verticalAlign: 'right'}}>
              {/* );
            })} */}
            </Grid>
          </React.Fragment>
        ) : (
          <CircularProgress />
        )}
      </Container>
    </React.Fragment>
  );
};
export default AirportInstance;
