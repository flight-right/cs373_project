import * as React from 'react';
import {DataGrid, GridColDef} from '@mui/x-data-grid';
import {Airline} from './AirportInstance';
import {Link} from 'react-router-dom';
import Button from '@mui/material/Button';

// // From https://reactrouter.com/web/api/Link
// function handleClick(props: string) {
//     <Link to='flightright.me/'
//   }

const columns: GridColDef[] = [
  {field: 'airline_name', headerName: 'Operating Airlines:', width: 175},
  {
    field: '',
    headerName: '',
    width: 75,
    renderCell: cellValues => {
      return (
        <Button>
          <Link to={`../../airlines/${cellValues.id}`}>View</Link>
        </Button>
      );
    },
  },
];

interface FlightTableProps {
  rows: Airline[];
}

export default function AirlineTable(props: FlightTableProps) {
  return (
    <div style={{height: 410, width: 300, marginBottom: 5}}>
      <DataGrid
        rows={props.rows}
        columns={columns}
        pageSize={5}
        rowsPerPageOptions={[5]}
      />
    </div>
  );
}
