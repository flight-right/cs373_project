import React from 'react';
import {geoCentroid} from 'd3-geo';
import {
  Annotation,
  ComposableMap,
  Geographies,
  Geography,
  ZoomableGroup,
  Marker,
} from 'react-simple-maps';
import {purple} from '@mui/material/colors';
import allStates from './data/allStates.json';

const geoUrl = 'https://cdn.jsdelivr.net/npm/us-atlas@3/states-10m.json';

const offsets = {
  VT: [50, -8],
  NH: [34, 2],
  MA: [30, -1],
  RI: [28, 2],
  CT: [35, 10],
  NJ: [34, 1],
  DE: [33, 0],
  MD: [47, 10],
  DC: [49, 21],
};

interface MapChartProps {
  latitude: number;
  longitude: number;
  name: string;
}

const MapChart = (props: MapChartProps) => {
  // order should be longtitude, latitude
  // https://github.com/zcreativelabs/react-simple-maps/issues/67
  const fromCoord: [number, number] = [props.longitude, props.latitude];

  const fromName = props.name;

  return (
    <ComposableMap
      projection="geoAlbersUsa"
      projectionConfig={{
        scale: 2000,
        rotate: [fromCoord[0], 0, 0],
        center: fromCoord,
      }}
    >
      <ZoomableGroup>
        <Geographies geography={geoUrl}>
          {({geographies}) => (
            <>
              {geographies.map(geo => (
                <Geography
                  key={geo.rsmKey}
                  stroke="#FFF"
                  geography={geo}
                  fill="#DDD"
                />
              ))}
              {geographies.map(geo => {
                const centroid = geoCentroid(geo);
                const cur = allStates.find(s => s.val === geo.id);
                return (
                  <g key={geo.rsmKey + '-name'}>
                    {cur &&
                      centroid[0] > -160 &&
                      centroid[0] < -67 &&
                      (Object.keys(offsets).indexOf(cur.id) === -1 ? (
                        <Marker coordinates={centroid}>
                          <text y="2" fontSize={12} textAnchor="middle">
                            {cur.id}
                          </text>
                        </Marker>
                      ) : (
                        <text x={4} fontSize={14} alignmentBaseline="middle">
                          {cur.id}
                        </text>
                      ))}
                  </g>
                );
              })}
              <Annotation subject={fromCoord} connectorProps={{strokeWidth: 0}}>
                <text
                  fontSize="20"
                  textAnchor="middle"
                  alignmentBaseline="middle"
                  fill={purple[400]}
                >
                  {fromName}
                </text>
              </Annotation>{' '}
              */
            </>
          )}
        </Geographies>
      </ZoomableGroup>
    </ComposableMap>
  );
};

export default MapChart;
