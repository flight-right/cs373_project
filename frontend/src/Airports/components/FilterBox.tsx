import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, {SelectChangeEvent} from '@mui/material/Select';
import {useHistory} from 'react-router-dom';
import React from 'react';

const FilterBoxCities = props => {
  const [city, setCity] = React.useState('');
  const history = useHistory();
  const currentLink = window.location.href.substring(
    window.location.href.lastIndexOf('/') + 1
  );
  const currentPage = props;

  const handleChange = (event: SelectChangeEvent) => {
    const {
      target: {value},
    } = event;

    if (window.location.href.includes('&city=')) {
      let newLink = currentLink.replace('&city=' + city, '&city=' + value);
      newLink = newLink.replace('&page=' + currentPage, '&page=1');
      history.push(newLink);
      setCity(value);
      location.reload();
      // <Redirect to={window.location.href + ',' + `${value}`} />;
    } else {
      history.push(currentLink + '&city=' + `${value}`);
      // <Redirect to={window.location.href + '&city' + `${value}`} />;
      setCity(value);
      location.reload();
    }
  };

  return (
    <Box sx={{minWidth: 120}}>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">City</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={city}
          label="city"
          onChange={handleChange}
        >
          <MenuItem value={'Apalachicola'}>Apalachicola</MenuItem>
          <MenuItem value={'Allentown'}>Allentown</MenuItem>
          <MenuItem value={'Abilene'}>Abilene</MenuItem>
          <MenuItem value={'Aliceville'}>Aliceville</MenuItem>
          <MenuItem value={'Akron'}>Akron</MenuItem>
        </Select>
      </FormControl>
    </Box>
  );
};

const FilterBoxStates = props => {
  const [state, setState] = React.useState('');
  const history = useHistory();
  const currentLink = window.location.href.substring(
    window.location.href.lastIndexOf('/') + 1
  );
  const currentPage = props;

  const handleChange = (event: SelectChangeEvent) => {
    const {
      target: {value},
    } = event;

    if (window.location.href.includes('&state=')) {
      let newLink = currentLink.replace('&state=' + state, '&state=' + value);
      newLink = newLink.replace('&page=' + currentPage, '&page=1');
      history.push(newLink);
      setState(value);
      // <Redirect to={window.location.href + ',' + `${value}`} />;
    } else {
      history.push(currentLink + '&state=' + `${value}`);
      // <Redirect to={window.location.href + '&city' + `${value}`} />;
      setState(value);
    }
  };

  return (
    <Box sx={{minWidth: 120}}>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">State</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={state}
          label="state"
          onChange={handleChange}
        >
          <MenuItem value={'Texas'}>Texas</MenuItem>
          <MenuItem value={'Florida'}>Florida</MenuItem>
          <MenuItem value={'Colorado'}>Colorado</MenuItem>
        </Select>
      </FormControl>
    </Box>
  );
};

export {FilterBoxCities, FilterBoxStates};
