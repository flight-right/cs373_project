import React, {useEffect, useState} from 'react';

import {CircularProgress} from '@material-ui/core';

import {
  AreaChart,
  Area,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer,
} from 'recharts';
import {fetchAllFlightData} from '../../Flights/fetchData';
import {Flight} from '../../Flights';

interface FlightTime {
  name: string;
  dept: number;
  arr: number;
}

const getFlightTimes = async () => {
  const times: FlightTime[] = [];

  for (let i = 0; i < 24; i++) {
    if (i < 10) {
      times.push({name: '0' + i + ':00', dept: 0, arr: 0});
    } else {
      times.push({name: i + ':00', dept: 0, arr: 0});
    }
  }

  const data = await fetchAllFlightData();
  data.rows.forEach((flight: Flight) => {
    const dept = flight.departure_time;
    const arr = flight.arrival_time;

    times[dept.hours()].dept++;
    times[arr.hours()].arr++;
  });

  return times;
};

function FlightTimes() {
  // const demoUrl = 'https://codesandbox.io/s/stacked-area-chart-ix341';

  const [times, setTimes] = useState<FlightTime[]>([]);
  const [loading, setLoading]: [boolean, (loading: boolean) => void] =
    React.useState<boolean>(true);

  useEffect(() => {
    const fetchData = async () => {
      const markersResponse = await getFlightTimes();
      setTimes(markersResponse);
      setLoading(false);
    };

    fetchData();
  }, []);

  if (loading) {
    return <CircularProgress />;
  }

  return (
    <ResponsiveContainer width={800} height={300}>
      <AreaChart
        width={500}
        height={300}
        data={times}
        margin={{
          top: 10,
          right: 30,
          left: 0,
          bottom: 0,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip />
        <Area
          type="monotone"
          dataKey="arr"
          stackId="1"
          stroke="#8884d8"
          fill="#8884d8"
        />
        <Area
          type="monotone"
          dataKey="dept"
          stackId="1"
          stroke="#82ca9d"
          fill="#82ca9d"
        />
        {/* <Area type="monotone" dataKey="amt" stackId="1" stroke="#ffc658" fill="#ffc658" /> */}
      </AreaChart>
    </ResponsiveContainer>
  );
}

export default FlightTimes;
