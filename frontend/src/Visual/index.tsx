import React, {useEffect, useState} from 'react';
import {Container} from '@mui/material';
// import * as d3 from 'd3';

import AirlineVisual from './components/AirlineVisual';
import TimeVisual from './components/TimeVisual';
import AirportVisual from './components/AirportVisual';

function Visual() {
  // const generateData = (value, length = 5) =>
  //   d3.range(length).map((item, index) => ({
  //     date: index,
  //     value:
  //       value === null || value === undefined ? Math.random() * 100 : value,
  //   }));

  const [data, setData] = useState<Array<{airport: string; volume: number}>>(
    []
  );

  useEffect(() => {
    setData([
      {airport: 'dogs', volume: 140},
      {airport: 'cats', volume: 91},
      {airport: 'hamsters', volume: 88},
      {airport: 'parrots', volume: 74},
      {airport: 'rabbits', volume: 63},
    ]);
  }, [!data]);

  return (
    <Container sx={{mt: 4, mb: 4}}>
      <AirlineVisual />
      <TimeVisual />
      <AirportVisual />
    </Container>
  );
}

export default Visual;
