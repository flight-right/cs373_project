import React, {useEffect, useState} from 'react';

import {
  Box,
  Container,
  createTheme,
  Grid,
  Link,
  Stack,
  ThemeProvider,
  Typography,
} from '@mui/material';
import {green, orange, purple, red} from '@mui/material/colors';
import {
  ArrowCircleDown,
  ArrowCircleUp,
  BugReport,
  Science,
} from '@mui/icons-material';
import {StatComponent, ToolComponent} from './components/StatComponent';
import {PersonCard} from './components/PersonCard';

import {fetchTeamData, PersonData, team} from './data/team';
import {api_data} from './data/apiData';

const themeOptions = createTheme({
  typography: {
    h1: {
      fontWeight: 700,
    },
    h4: {
      fontWeight: 700,
    },
    h5: {
      fontWeight: 700,
    },
  },
});

function Index() {
  const [teamData, setTeamData] = useState<Array<PersonData>>(team);
  const [issueCount, setIssues] = useState<number>(0);
  const [commitCount, setCommit] = useState<number>(0);
  const [mrCount, setMR] = useState<number>(0);
  const [testCount, setTests] = useState<number>(0);
  const [hasItLoaded, finished] = useState<boolean>(false);

  useEffect(() => {
    document.title = 'About - Flight Right';
    const setStates = async () => {
      const retrieve = await fetchTeamData();
      setTeamData(retrieve.teamData);
      setIssues(retrieve.issueCount);
      setMR(retrieve.mrCount);
      setTests(retrieve.testCount);
      setCommit(retrieve.commitCount);
      finished(true);
    };

    setStates();
  }, []);

  const api_display = () =>
    api_data.map(data => (
      <li key={'api_data_' + data.name}>
        {data.description} by <Link href={data.url}>{data.name}</Link>
      </li>
    ));

  const person_display = (person_data: Array<PersonData>) => {
    return person_data.map(data => (
      <PersonCard
        key={'person_card_' + data.name}
        name={data.name}
        role={data.role}
        bio={data.bio}
        img={data.img}
        gitlab={data.gitlab}
        commits={hasItLoaded ? data.commits : -1}
        issues={hasItLoaded ? data.issues : -1}
        tests={hasItLoaded ? data.tests : -1}
      />
    ));
  };

  return (
    <ThemeProvider theme={themeOptions}>
      <Container>
        <Typography
          variant={'h1'}
          textAlign={'center'}
          fontWeight={'bold'}
          gutterBottom
          sx={{mt: 12, mb: 8}}
        >
          Flight Right
        </Typography>

        <Box sx={{m: 'auto', mb: 12, maxWidth: 700}}>
          <Typography variant={'body1'} gutterBottom>
            Our website aims to help users obtain information regarding their
            next flight. Users will be able to see upcoming flights within the
            U.S., airport information, and airline company information. Our goal
            is to help make searching for flight information as seamless as
            possible. FlightRight aims to answer questions such as the
            following: Which airports are the most reliable? What is the status
            of an ongoing flight? What is the price difference between two
            different plane tickets? Whether it’s checking for airline company
            COVID-19 protocols or baggage policies, or simply finding the
            cheapest airplane ticket to a destination, FlightRight has our
            customers covered.
          </Typography>

          <Typography variant={'body1'} gutterBottom>
            Integrating many different APIs allows users to see existing data in
            a fresh new perspective. FlightRight allows users to see flights
            from specific airlines and airports along with current flights.
            Allowing users to compare this data offers a unique perspective into
            existing flight data.
          </Typography>
        </Box>

        <Box mb={12}>
          <Typography variant={'h4'} textAlign={'center'} gutterBottom>
            Repo Stats
          </Typography>
          <Grid container sx={{mb: 4}} spacing={4} justifyContent={'center'}>
            <StatComponent
              title={hasItLoaded ? commitCount : '...'}
              text={'Commits'}
              color={purple[500]}
              tooltip={'total number of commits to all branches'}
              icon={<ArrowCircleUp fontSize={'large'} />}
            />

            <StatComponent
              title={hasItLoaded ? issueCount : '...'}
              text={'Issues'}
              color={red[500]}
              tooltip={'total number of opened issues'}
              icon={<BugReport fontSize={'large'} />}
            />

            <StatComponent
              title={hasItLoaded ? mrCount : '...'}
              text={'MRs'}
              color={orange[500]}
              tooltip={'total number of merge requests closed'}
              icon={<ArrowCircleDown fontSize={'large'} />}
            />

            <StatComponent
              title={hasItLoaded ? testCount : '...'}
              text={'Tests'}
              color={green[500]}
              tooltip={'total number of tests written'}
              icon={<Science fontSize={'large'} />}
            />
          </Grid>

          <Grid container spacing={4} justifyContent={'center'}>
            {person_display(teamData)}
          </Grid>
        </Box>

        <Stack mb={12}>
          <Box sx={{m: {sm: 'auto'}, display: 'inline-block'}}>
            <Typography variant={'h4'} textAlign={{sm: 'center'}} gutterBottom>
              APIs
            </Typography>
            <ul>{api_display()}</ul>
          </Box>
        </Stack>

        <Box mb={8}>
          <Typography variant={'h4'} textAlign={{sm: 'center'}} gutterBottom>
            Tools
          </Typography>
          <Grid container spacing={2} justifyContent={{sm: 'center'}}>
            <ToolComponent
              title={'GitLab'}
              text={'Version Control'}
              color={'#554488'}
              img={
                'https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png'
              }
              href={'https://gitlab.com/'}
            />

            <ToolComponent
              title={'React'}
              text={'Frontend Framework'}
              color={'#ffffff'}
              img={
                'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9Ii0xMS41IC0xMC4yMzE3NCAyMyAyMC40NjM0OCI+CiAgPHRpdGxlPlJlYWN0IExvZ288L3RpdGxlPgogIDxjaXJjbGUgY3g9IjAiIGN5PSIwIiByPSIyLjA1IiBmaWxsPSIjNjFkYWZiIi8+CiAgPGcgc3Ryb2tlPSIjNjFkYWZiIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiPgogICAgPGVsbGlwc2Ugcng9IjExIiByeT0iNC4yIi8+CiAgICA8ZWxsaXBzZSByeD0iMTEiIHJ5PSI0LjIiIHRyYW5zZm9ybT0icm90YXRlKDYwKSIvPgogICAgPGVsbGlwc2Ugcng9IjExIiByeT0iNC4yIiB0cmFuc2Zvcm09InJvdGF0ZSgxMjApIi8+CiAgPC9nPgo8L3N2Zz4K'
              }
              href={'https://react.com/'}
            />

            <ToolComponent
              title={'Namecheap'}
              text={'Domain Hosting'}
              // color={"#"}
              img={
                'https://toppng.com/uploads/preview/namecheap-logo-11609369922hooobefgfj.png'
              }
              href={'https://namecheap.com/'}
            />

            <ToolComponent
              title={'Discord'}
              text={'Team Communication'}
              // color={"#5865F2"}
              img={
                'https://www.freepnglogos.com/uploads/discord-logo-png/concours-discord-cartes-voeux-fortnite-france-6.png'
              }
              href={'https://discord.com/'}
            />

            <ToolComponent
              title={'Postman'}
              text={'API Testing and Documentation'}
              // color={"#"}
              img={
                'https://www.postman.com/assets/logos/postman-logo-stacked.svg'
              }
              href={'https://www.postman.com/'}
            />

            <ToolComponent
              title={'Material UI'}
              text={'Frontend Component Library'}
              // color={"#"}
              img={'https://media.zeemly.com/zeemly/product/material-ui.png'}
              href={'https://mui.com/'}
            />

            <ToolComponent
              title={'AWS'}
              text={'Hosting Service'}
              // color={"#ff9900"}
              img={
                'https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Amazon_Web_Services_Logo.svg/1280px-Amazon_Web_Services_Logo.svg.png'
              }
              href={'https://aws.amazon.com/'}
            />

            <ToolComponent
              title={'VSCode'}
              text={'Code Editor'}
              // color={"#"}
              img={
                'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Visual_Studio_Code_1.35_icon.svg/2048px-Visual_Studio_Code_1.35_icon.svg.png'
              }
              href={'https://code.visualstudio.com/'}
            />

            {/*<ToolComponent*/}
            {/*  title={""}*/}
            {/*  text={""}*/}
            {/*  color={"#"}*/}
            {/*  img={""}*/}
            {/*  href={""}*/}
            {/*/>*/}
          </Grid>
        </Box>
      </Container>
    </ThemeProvider>
  );
}

export default Index;
