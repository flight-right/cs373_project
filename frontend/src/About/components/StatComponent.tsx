import React from 'react';
import {
  Avatar,
  Box,
  ButtonBase,
  Grid,
  Paper,
  Skeleton,
  Stack,
  Tooltip,
  Typography,
} from '@mui/material';
import LaunchIcon from '@mui/icons-material/Launch';

interface CardComponentProps {
  title: string | number;
  text: string;
  color?: string;
  icon?: React.ReactElement;
  img?: string;
  href?: string;
  tooltip?: string;
}

export function StatComponent(props: CardComponentProps) {
  const {title, text, color, icon} = props;

  return (
    <Grid item md={2} sm={3} xs={6}>
      <Stack
        direction={{xs: 'row', s: 'column'}}
        spacing={2}
        alignItems={'center'}
      >
        <Box mt={1}>
          {icon && (
            <Avatar
              sx={{
                width: 50,
                height: 50,
                bgcolor: color ? color : 'black',
              }}
              variant={'rounded'}
            >
              {icon}
            </Avatar>
          )}
        </Box>
        <Box>
          <Typography variant={'h5'}>
            {title === '...' ? <Skeleton /> : title}
          </Typography>
          {props.tooltip ? (
            <Tooltip title={props.tooltip}>
              <Typography variant="body2" color={'text.secondary'}>
                {text}
              </Typography>
            </Tooltip>
          ) : (
            <Typography variant="body2" color={'text.secondary'}>
              {text}
            </Typography>
          )}
        </Box>
      </Stack>
    </Grid>
  );
}

export function ToolComponent(props: CardComponentProps) {
  const {title, text, color, icon} = props;

  return (
    <Grid item>
      <ButtonBase href={props.href || ''} target="_blank">
        <Paper sx={{p: 2}}>
          <Stack
            direction={{xs: 'row', s: 'column'}}
            spacing={2}
            alignItems={'center'}
          >
            <Box>
              {(icon || props.img) && (
                <Avatar
                  sx={{
                    width: 40,
                    height: 'auto',
                    bgcolor: color ? color : 'transparent',
                  }}
                  src={props.img}
                  variant={'rounded'}
                >
                  {icon}
                </Avatar>
              )}
            </Box>
            <Box>
              <Typography variant={'subtitle2'}>
                {title} <LaunchIcon sx={{fontSize: 10}} />
              </Typography>
              <Typography variant="body2" color={'text.secondary'}>
                {text}
              </Typography>
            </Box>
          </Stack>
        </Paper>
      </ButtonBase>
    </Grid>
  );
}
