import {
  Card,
  CardContent,
  CardMedia,
  Grid,
  Link,
  Skeleton,
  Stack,
  Tooltip,
  Typography,
} from '@mui/material';
import {ArrowCircleUp, BugReport, Science} from '@mui/icons-material';
import React from 'react';

interface PersonCardProps {
  name: string;
  role: string;
  bio: string;
  img: string;
  gitlab: string;
  commits: number;
  issues: number;
  tests: number;
}

export function PersonCard(props: PersonCardProps) {
  return (
    <Grid item>
      <Card sx={{maxWidth: 300}}>
        <CardMedia
          component="img"
          image={props.img}
          alt={props.name + "'s picture"}
          sx={{borderRadius: 50, m: 'auto', width: '50%', mt: 4}}
        />
        <CardContent sx={{textAlign: 'center'}}>
          <Link href={`https://gitlab.com/${props.gitlab}`}>
            @{props.gitlab}
          </Link>
          <Typography variant="h5">{props.name}</Typography>
          <Typography gutterBottom variant={'subtitle1'} color="text.secondary">
            {props.role}
          </Typography>
          <Typography variant="body2" color={'text.secondary'}>
            {props.bio}
          </Typography>
          <Stack
            direction={'row'}
            justifyContent="space-around"
            spacing={1}
            sx={{mt: 2, color: 'text.secondary'}}
          >
            <Stack alignItems={'center'} spacing={0.5}>
              <Tooltip title={`commits by ${props.name}`}>
                <ArrowCircleUp />
              </Tooltip>
              {props.commits >= 0 ? (
                props.commits
              ) : (
                <Skeleton variant={'text'} width={20} />
              )}
            </Stack>
            <Stack alignItems={'center'} spacing={0.5}>
              <Tooltip title={`issues by ${props.name}`}>
                <BugReport />
              </Tooltip>
              {props.commits >= 0 ? (
                props.issues
              ) : (
                <Skeleton variant={'text'} width={20} />
              )}
            </Stack>
            <Stack alignItems={'center'} spacing={0.5}>
              <Tooltip title={`test cases by ${props.name}`}>
                <Science />
              </Tooltip>
              {props.commits >= 0 ? (
                props.tests
              ) : (
                <Skeleton variant={'text'} width={20} />
              )}
            </Stack>
          </Stack>
        </CardContent>
      </Card>
    </Grid>
  );
}
