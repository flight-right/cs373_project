export const api_data = [
  {
    name: 'AreoDataBox',
    description: 'Flights from airports and flight data',
    url: 'https://doc.aerodatabox.com/',
  },
  {
    name: 'Airport Info',
    description: 'Airport data',
    url: 'https://rapidapi.com/Active-api/api/airport-info/',
  },
  {
    name: 'AviationStack',
    description: 'Airport and live flight data',
    url: 'https://aviationstack.com/documentation ',
  },
  {
    name: 'OpenSky',
    description: 'Data on live flights',
    url: 'https://opensky-network.org/apidoc/rest.html ',
  },
  {
    name: 'FlightStats',
    description: 'Flight and misc. Data (weather, delay index)',
    url: 'https://developer.flightstats.com/products',
  },
  {
    name: 'Proground',
    description: 'Airline data by code',
    url: 'https://rapidapi.com/proground/api/aviation-reference-data ',
  },
  {
    name: 'Airport bag history',
    description: 'Airport bag history',
    url: 'https://www.developer.aero/api-catalog/bagjourney-api#/',
  },
  {
    name: 'Google Images',
    description: 'Google image API',
    url: ' https://serpapi.com/images-results',
  },
  {
    name: 'Google Maps',
    description: 'Google maps API',
    url: 'https://developers.google.com/maps ',
  },
  {
    name: 'MediaStack',
    description: 'Airline News API',
    url: 'https://mediastack.com/',
  },
  {
    name: 'Alpha Advantage',
    description: 'Stock API',
    url: 'https://www.alphavantage.co/',
  },
  {
    name: 'Clearbit Logos',
    description: 'Logo API',
    url: 'https://clearbit.com/docs#logo-api',
  },
];
