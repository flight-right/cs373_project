export interface PersonData {
  name: string;
  gitlab: string;
  role: string;
  bio: string;
  img: string;
  commits: number;
  issues: number;
  tests: number;
}

export const team = [
  {
    name: 'Lucas Gates',
    gitlab: 'LucasG234',
    role: 'Devops Engineer',
    bio: 'My name is Lucas Gates and I am a Junior here at UT Austin studying Computer Science and Business.',
    img: 'profile/lucas-profile.jpg',
    commits: 0,
    issues: 0,
    tests: 15,
  },
  {
    name: 'Yash Patil',
    gitlab: 'YashPat',
    role: 'Backend Developer',
    bio: 'My name is Yash Patil, and I am a senior studying Computer Science.',
    img: 'profile/yash-profile.png',
    commits: 0,
    issues: 0,
    tests: 0,
  },
  {
    name: 'Pranav Rayudu',
    gitlab: 'PranavRayudu',
    role: 'Frontend Developer',
    bio: 'My name is Pranav Rayudu and I am a junior at UT Austin studying Computer Science.',
    img: 'profile/pranav-profile.jpg',
    commits: 0,
    issues: 0,
    tests: 20,
  },
  {
    name: 'Craig Leeson',
    gitlab: 'craig.leeson',
    role: 'Frontend Developer',
    bio: 'I’m a senior studying Computer Science at The University of Texas. I love playing ultimate frisbee as a hobby. ',
    img: 'profile/craig-profile.jpg',
    commits: 0,
    issues: 0,
    tests: 16,
  },
  {
    name: 'Minho Lee',
    gitlab: 'minMaxLee',
    role: 'Frontend Developer',
    bio: 'I’m Minho and I am a senior at UT Austin studying computer science and math.',
    img: 'profile/minho-profile.jpg',
    commits: 0,
    issues: 0,
    tests: 0,
  },
];

export async function fetchTeamData() {
  const REPO_URL = 'https://gitlab.com/api/v4/projects/29920878/';
  const stats = await fetch(
    REPO_URL + 'repository/contributors?per_page=900&page=1'
  );
  const statData = await stats.json();
  let totalCommits = 0;
  /* eslint-disable @typescript-eslint/no-explicit-any*/
  statData.forEach((element: any) => {
    team.forEach(person => {
      if (
        person.name === element.name ||
        (person.name === 'Minho Lee' && element.name === 'Minho')
      ) {
        person.commits = element.commits;
        totalCommits += element.commits;
      }
    });
  });

  let pageNum = '1';
  let count = 0;
  let totalIssues = 0;
  do {
    count = 0;
    const issues = await fetch(
      REPO_URL + 'issues?state=closed&per_page=400&page=' + pageNum
    );
    console.log(REPO_URL + 'issues?state=closed&per_page=400&page=' + pageNum);
    const issueData = await issues.json();
    issueData.forEach((element: any) => {
      totalIssues += 1;
      team.forEach(person => {
        if (person.gitlab === element.author.username) {
          person.issues += 1;
        }
      });
      count += 1;
    });
    let convert = parseInt(pageNum);
    convert += 1;
    pageNum = convert.toString();
  } while (count > 0);

  /*
  const issues = await fetch(
    REPO_URL + 'issues?state=closed&per_page=900&page=1'
  );
  const issueData = await issues.json();
  let totalIssues = 0;
  issueData.forEach((element: any) => {
    element.assignees.forEach((assignee: any) => {
      team.forEach(person => {
        if (person.gitlab === assignee.username) {
          person.issues += 1;
        }
      });
    });
  });*/

  const mergeRequests = await fetch(
    REPO_URL + 'merge_requests?state=closed&per_page=900&page=1'
  );
  const mrData = await mergeRequests.json();
  const totalMRs = mrData.length;

  let totalTests = 0;
  team.forEach(person => {
    totalTests += person.tests;
  });

  return {
    teamData: team,
    issueCount: totalIssues,
    commitCount: totalCommits,
    mrCount: totalMRs,
    testCount: totalTests,
  };
}
