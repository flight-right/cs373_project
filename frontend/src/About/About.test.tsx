import React from 'react';
import {render, screen} from '@testing-library/react';
import {shallow} from 'enzyme';
import About from './index';
import {PersonCard} from './components/PersonCard';

it('renders links to repo and api docs', () => {
  render(<About />);

  // https://stackoverflow.com/questions/57827126/how-to-test-anchors-href-with-react-testing-library
  const title = screen.getByText('Flight Right');
  expect(title).toBeInTheDocument();

  const yeet = screen.getByText('Flight Right');
  expect(yeet).toBeInTheDocument();
});

it('renders all team members', () => {
  const wrapper = shallow(<About />);
  expect(wrapper.find(PersonCard)).toHaveLength(5);
});
