import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';

interface CardInfo {
  height: number;
  width: number;
  cardTitle: string;
}

export default function TextCard(props: CardInfo) {
  return (
    <Card sx={{width: props.width, height: props.height, marginBottom: 1}}>
      <CardContent>
        <Typography variant="h5" component="div">
          {props.cardTitle}
        </Typography>
        <Typography sx={{mb: 1.5}} color="text.secondary">
          adjective
        </Typography>
        <Typography variant="body2">
          well meaning and kindly.
          <br />
          {'"a benevolent smile"'}
        </Typography>
      </CardContent>
    </Card>
  );
}
