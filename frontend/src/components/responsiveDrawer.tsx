import * as React from 'react';
import Box from '@mui/material/Box';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import {IconButton, ListItemButton} from '@mui/material';
import {ArrowBack, Menu} from '@mui/icons-material';
import HomeIcon from '@mui/icons-material/Home';
import Divider from '@mui/material/Divider';
import FlightIcon from '@mui/icons-material/Flight';
import FlightTakeoffIcon from '@mui/icons-material/FlightTakeoff';
import AirplaneTicketIcon from '@mui/icons-material/AirplaneTicket';
import InfoIcon from '@mui/icons-material/Info';
import Drawer from '@mui/material/Drawer';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';

const drawerWidth = 300;

interface ListItemLinkProps {
  icon?: React.ReactElement;
  /* eslint-disable @typescript-eslint/no-explicit-any*/
  primary: any;
  to: string;
}

function ListItemLink(props: ListItemLinkProps) {
  const {icon, primary, to} = props;

  return (
    <ListItem>
      <ListItemButton component={'a'} href={to}>
        {icon && <ListItemIcon>{icon}</ListItemIcon>}
        <ListItemText primary={primary} />
      </ListItemButton>
    </ListItem>
  );
}

export function ResponsiveDrawer() {
  const [state, setState] = React.useState(false);

  const toggleDrawer =
    (open: boolean) => (event: React.KeyboardEvent | React.MouseEvent) => {
      if (
        event.type === 'keydown' &&
        ((event as React.KeyboardEvent).key === 'Tab' ||
          (event as React.KeyboardEvent).key === 'Shift')
      ) {
        return;
      }

      setState(open);
    };

  const list = () => {
    return (
      <Box
        sx={{
          width: drawerWidth,
        }}
        role="presentation"
        onClick={toggleDrawer(false)}
        onKeyDown={toggleDrawer(false)}
      >
        <List>
          <ListItem>
            <IconButton
              size="large"
              color="inherit"
              aria-label="menu"
              onClick={toggleDrawer(false)}
              sx={{mr: 2}}
            >
              <ArrowBack />
            </IconButton>
          </ListItem>
          <ListItemLink to={'/'} primary={'Home'} icon={<HomeIcon />} />
          <Divider />
          <ListItemLink
            to={'/flights'}
            primary={'Flights'}
            icon={<FlightIcon />}
          />
          <ListItemLink
            to={'/airports'}
            primary={'Airports'}
            icon={<FlightTakeoffIcon />}
          />
          <ListItemLink
            to={'/flights'}
            primary={'Flights'}
            icon={<AirplaneTicketIcon />}
          />
          <Divider />
          <ListItemLink to={'/about'} primary={'About'} icon={<InfoIcon />} />
        </List>
      </Box>
    );
  };

  return (
    <>
      <IconButton
        size="large"
        edge="start"
        color="inherit"
        aria-label="menu"
        onClick={toggleDrawer(true)}
        sx={{
          display: {xs: 'inherit', sm: 'none'},
          // '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          mr: 2,
        }}
      >
        <Menu />
      </IconButton>

      <Drawer
        anchor={'left'}
        variant={'temporary'}
        open={state}
        onClose={toggleDrawer(false)}
        ModalProps={{keepMounted: true}} // Better open performance on mobile.
        sx={{
          display: {xs: 'block', sm: 'none'},
        }}
      >
        {list()}
      </Drawer>
    </>
  );
}
