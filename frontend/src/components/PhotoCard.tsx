import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';

type CardInfo = {
  height: number;
  maxWidth: number;
  albumId: number;
  name: string;
};

export default function ActionAreaCard(props: CardInfo) {
  return (
    <Card sx={{maxWidth: props.maxWidth, height: props.height}}>
      <CardMedia
        component="img"
        height="100"
        // image="https://upload.wikimedia.org/wikipedia/en/thumb/2/23/American_Airlines_logo_2013.svg/300px-American_Airlines_logo_2013.svg.png"
        alt="PLACEHOLDER_IMAGE"
        sx={{objectFit: 'scale-down'}}
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {/* Airline Name */}
          {props.name} {props.albumId}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          test
        </Typography>
      </CardContent>
    </Card>
  );
}
