import React from 'react';
import {Container, Grid, Link} from '@mui/material';

function Footer() {
  return (
    <Container
      sx={{
        mt: 4,
        mb: 4,
      }}
    >
      <Grid
        container
        spacing={{xs: 1, sm: 2}}
        direction={{xs: 'column', sm: 'row'}}
      >
        <Grid item xs>
          &copy; 2021 Flight Right Project.
        </Grid>
        <Grid item>
          <Link
            href={'https://gitlab.com/flight-right/cs373_project'}
            color={'#000'}
          >
            Repo
          </Link>
        </Grid>
        <Grid item>
          <Link
            href={'https://documenter.getpostman.com/view/17755692/UUy4bjoc'}
            color={'#000'}
          >
            API Docs
          </Link>
        </Grid>
      </Grid>
    </Container>
  );
}

export default Footer;
