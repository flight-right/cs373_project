import * as React from 'react';
import Box from '@mui/material/Box';
import {
  AppBar,
  Container,
  Link,
  Stack,
  Toolbar,
  Typography,
  Button,
} from '@mui/material';
import styled from 'styled-components';
import {ResponsiveDrawer} from './responsiveDrawer';

function TitleName() {
  return (
    <Box sx={{flexGrow: 1}}>
      <Link
        href={'/'}
        underline={'none'}
        color={'black'}
        display={'inline-block'}
      >
        <Stack direction={'row'} spacing={2}>
          <img
            alt={'Flight Right Logo'}
            height={32}
            src={
              'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgZGF0YS1uYW1lPSJMYXllciAyIiBpZD0iTGF5ZXJfMiIgdmlld0JveD0iMCAwIDY0IDY0IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxkZWZzPjxzdHlsZT4uY2xzLTF7ZmlsbDojZmZlOTRmO30uY2xzLTJ7ZmlsbDojNDVkNGQ5O30uY2xzLTN7ZmlsbDojMmQ5NWI1O30uY2xzLTR7ZmlsbDojZjc0YjUwO30uY2xzLTV7ZmlsbDojMmMyYTNkO308L3N0eWxlPjwvZGVmcz48dGl0bGUvPjxwb2x5Z29uIGNsYXNzPSJjbHMtMSIgcG9pbnRzPSI2MSA4IDMgMjcuODEgMjAuNjEgMzYuMzcgMjUuNDUgNTAuOTIgNDAuMzMgNDUuOTYgNDAuMzMgNDUuOTYgNjEgNTYgNjEgOCIvPjxwb2x5Z29uIGNsYXNzPSJjbHMtMiIgcG9pbnRzPSI2MSA4IDIwLjYxIDM2LjM3IDI1LjQ1IDUwLjkyIDYxIDgiLz48cG9seWdvbiBjbGFzcz0iY2xzLTMiIHBvaW50cz0iMjAuNjEgMzYuMzcgMjIuMzIgNDEuNTIgNjEgOCAyMC42MSAzNi4zNyIvPjxwb2x5Z29uIGNsYXNzPSJjbHMtNCIgcG9pbnRzPSI2MSA4IDI1LjQ1IDUwLjkyIDQwLjMzIDQ1Ljk2IDYxIDgiLz48cGF0aCBjbGFzcz0iY2xzLTUiIGQ9Ik02Miw3LjkzYS4yLjIsMCwwLDAsMC0uMDd2MHMwLDAsMCwwYS4wOS4wOSwwLDAsMCwwLDBzMCwwLDAtLjA1YS41OC41OCwwLDAsMCwwLS4xNGgwYS4xOC4xOCwwLDAsMC0uMDUtLjA3LjA2LjA2LDAsMCwwLDAsMGwwLDAsMCwwLS4xMS0uMSwwLDBoMGwwLDAsMCwwLDAsMHMwLDAsMCwwaDBsLS4xMy0uMDZoLS4wNmwwLDBoLS4zbC0uMDcsMGgtLjA4bDAsMGgwbC01OCwxOS44MmExLDEsMCwwLDAtLjEyLDEuODRsMTcuMjMsOC4zN0wyNC41LDUxLjI0YS40My40MywwLDAsMSwwLC4wNy44Ni44NiwwLDAsMCwuMS4xNiwxLDEsMCwwLDAsLjEyLjE2bDAsLjA2LjEyLjA3LjA3LjA2aC4wNmEuODYuODYsMCwwLDAsLjIzLjA2bC4xMiwwaDBsLjEzLDBhMS4yNSwxLjI1LDAsMCwwLC4xOSwwTDQwLjI2LDQ3bDIwLjMsOS44N0ExLDEsMCwwLDAsNjEsNTdhMSwxLDAsMCwwLDEtMVY4UzYyLDgsNjIsNy45M1pNMzkuNjQsNDUuMTMsMjguNDUsNDguODcsNTUuMTgsMTYuNTlaTTIxLjc5LDM2Ljc2LDU1LjQ4LDEzLjEsMjUuODMsNDguOVpNNTQuNjgsMTEuMjFsLTM0LjE3LDI0TDUuNjIsMjhabS0xMywzNC4zTDYwLDExLjkzVjU0LjRaIi8+PC9zdmc+'
            }
          />
          <Typography variant={'h6'}>Flight Right</Typography>
        </Stack>
      </Link>
    </Box>
  );
}

const Nav = styled.nav`
  background: white;
`;

export default function Navbar() {
  return (
    <Nav>
      {/*https://stackoverflow.com/questions/50117231/transparent-appbar-in-material-ui-react*/}
      <AppBar position={'static'} color="transparent" elevation={0}>
        <Container>
          <Toolbar disableGutters={true}>
            <ResponsiveDrawer />
            {TitleName()}

            <Box sx={{display: {xs: 'none', sm: 'block'}}}>
              <Button color={'inherit'} href={'/flights'}>
                Flights
              </Button>
              <Button color={'inherit'} href={'/airports/limit=12&page=1'}>
                Airports
              </Button>
              <Button color={'inherit'} href={'/airlines/limit=9&page=1'}>
                Airlines
              </Button>
              <Button color={'inherit'} href={'/search'}>
                Search
              </Button>
              <Button color={'inherit'} href={'/visual'} sx={{ml: 4}}>
                Visual
              </Button>
              <Button color={'inherit'} href={'/provider'} sx={{ml: 4}}>
                Provider Visual
              </Button>
              <Button color={'inherit'} href={'/about'} sx={{ml: 4}}>
                About
              </Button>
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
    </Nav>
  );
}
