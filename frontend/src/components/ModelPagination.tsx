import * as React from 'react';
import Pagination from '@mui/material/Pagination';

interface ModelPageInfo {
  itemsPerPage: number;
  totalItems: number;
  handleChange: (event: React.ChangeEvent<unknown>, page: number) => void;
  page: number;
}

const ModelPagination = ({
  itemsPerPage,
  totalItems,
  handleChange,
  page,
}: ModelPageInfo) => {
  const pageNumbers: number[] = [];
  for (let i = 1; i <= Math.ceil(totalItems / itemsPerPage); i++) {
    pageNumbers.push(i);
  }

  return (
    <Pagination
      shape="rounded"
      count={pageNumbers.length}
      onChange={handleChange}
      page={page}
    />
  );
};

export default ModelPagination;
