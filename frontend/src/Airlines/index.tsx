import React, {useEffect, useState} from 'react';
import {Container, Grid} from '@mui/material';
import axios from 'axios';
import AirlineModelCard from './components/AirlineModelCard';
import ModelPagination from '../components/ModelPagination';
import {
  Breadcrumbs,
  LinearProgress,
  Select,
  MenuItem,
  InputLabel,
  FormControl,
  Typography,
  TextField,
  Button,
  Link,
} from '@mui/material';
import {RouteComponentProps, useHistory} from 'react-router-dom';
import {countries} from './components/countryList';

export interface Flight {
  id: number;
  flight_number: string;
  // model: string;
  // cost: number;
  departure_time: moment.Moment;
  arrival_time: moment.Moment;
  departure_airport: Airport;
  arrival_airport: Airport;
  airline: Airline;
  duration: number;
  status: string;
}

export interface AirlinePranav {
  airline_name: string;
  airline_country?: string;
  airline_av_fleet?: number;
  airline_fleet_size?: number;
  airline_founded?: number;
  airline_hub_code?: string;
  airline_iata?: string;
  airline_icao?: string;
  id?: number;
}

export interface FlightAPI {
  airline?: AirlineAPI;
  airport?: AirportAPI;
  flight_aircraft_img: string; // url?
  flight_airline_name: string;
  flight_arr_airport: string;
  flight_arr_airport_iata: string;
  flight_arr_airport_icao: string;
  flight_arr_time: string;
  flight_dept_airport: string;
  flight_dept_airport_iata: string;
  flight_dept_airport_icao: string;
  flight_dept_time: string;
  flight_iata: string;
  flight_icao: string;
  flight_lat: number;
  flight_long: number;
  flight_number: string;
  flight_status: string;
  id: number;
}

export interface AirportAPI {
  airlines: AirlineAPI[];
  airport_address: string;
  airport_city: string;
  airport_flight_volume: 0;
  airport_iata: string;
  airport_icao: string;
  airport_lat: number;
  airport_long: number;
  airport_maps: string;
  airport_name: string;
  airport_phone_number: string;
  airport_rating: number;
  airport_runway_number: number;
  airport_state: string;
  airport_timezone: string;
  airport_twitter: string;
  airport_website: string;
  flights: FlightAPI[];
  id: number;
}

export interface AllAPIData<Type> {
  count: number;
  data: Type[];
}

export interface Airport {
  id: number;
  name: string;
  code: string;
  lat: number;
  lon: number;
}

interface AirlineAPI {
  count: number;
  data: Airline[];
  total: number;
}

interface Airline {
  airline_name: string;
  airline_country: string;
  airline_fleet_size: number;
  airline_founded: number;
  airline_hub_code: string;
  airline_iata: string;
  airline_icao: string;
  airline_logo: string;
  id: number;
}

const defaultAirlines: Airline[] = [];

const Airlines = ({
  match,
}: RouteComponentProps<{
  limit: string;
  page: string;
  sort: string;
  filter: string;
  search: string;
}>) => {
  const [airlines, setAirlines]: [Airline[], (airlines: Airline[]) => void] =
    useState(defaultAirlines);
  const [loading, setLoading] = useState(false);
  // console.log(match.params.page);
  const [currentPage, setCurrentPage] = useState(
    match.params.page.includes('&sort_by=') ||
      match.params.page.includes('&country=')
      ? Number(
          match.params.page.substring(0, match.params.page.indexOf('&country'))
        )
      : Number(match.params.page)
  );
  const [itemsPerPage] = useState(Number(match.params.limit));
  const [, setTotalAirlinesFound] = useState(0);
  const [airlinesFound, setAirlinesFound] = useState(0);

  const [sort] = useState(
    match.params.page.includes('&sort_by=')
      ? match.params.page.includes('&country=')
        ? match.params.page.substring(
            match.params.page.indexOf('&sort_by='),
            match.params.page.indexOf('&country=')
          )
        : match.params.page.substring(match.params.page.indexOf('&sort_by='))
      : ''
  );

  const [search, setSearch] = useState(
    match.params.page.includes('&search_term=')
      ? match.params.page.substring(match.params.page.indexOf('&search_term='))
      : ''
  );

  const toHighlight = search === '' || search === ' ' ? false : true;

  const [filter] = useState(
    match.params.page.includes('&country=')
      ? match.params.page.substring(match.params.page.indexOf('&country='))
      : ''
  );

  const history = useHistory();
  const currentLink = window.location.href.substring(
    window.location.href.lastIndexOf('/') + 1
  );

  useEffect(() => {
    fetchAirlines(match.params.limit, match.params.page, sort, filter, search);
  }, []);

  const fetchAirlines = (
    limit: string,
    page: string,
    sort: string,
    filter: string,
    search: string
  ) => {
    setLoading(true);
    axios
      .get<AirlineAPI>(
        `https://api.flightright.me/airlines?limit=${limit}&page=${page}${sort}${filter}${search}`,
        {
          headers: {
            'Content-Type': 'application/json',
          },
        }
      )
      .then(res => {
        console.log(res);
        console.log('CURRENT PAGE BEFORE CHANGE:' + currentPage);
        console.log('SORTING BY:' + sort);
        console.log('FILTERING BY:' + filter);
        console.log('SEARCH BY:' + search);
        setTotalAirlinesFound(res.data.total);
        setAirlines(res.data.data);
        setLoading(false);
      })
      .catch(err => {
        console.log(err);
      });

    axios
      .get<AirlineAPI>(
        `https://api.flightright.me/airlines?${sort}${filter}${search}`,
        {
          headers: {
            'Content-Type': 'application/json',
          },
        }
      )
      .then(res => {
        setAirlinesFound(res.data.count);
        setLoading(false);
      });
  };

  // Change page
  const handleChange = (
    event: React.ChangeEvent<unknown>,
    pageNumber: number
  ) => {
    // // const before = currentLink.substring(0, currentLink.indexOf('&page'));
    // // const after = currentLink.substring(currentLink.indexOf('&page') + 7);
    // currentLink.replace('page=' + currentPage, 'page=' + pageNumber);
    history.push(
      currentLink.replace('&page=' + currentPage, '&page=' + pageNumber)
    );

    fetchAirlines('' + itemsPerPage, '' + pageNumber, sort, filter, search);
    setCurrentPage(pageNumber);
  };

  // Handles the searching for model page
  const handleSearch = () => {
    const searchTerms =
      document.getElementById('search-box') === null
        ? ''
        : (document.getElementById('search-box')! as HTMLInputElement).value;

    setSearch(
      (document.getElementById('search-box')! as HTMLInputElement).value
    );

    history.push(
      currentLink.substring(0, currentLink.indexOf('&page=' + currentPage)) +
        '&page=1&search_term=' +
        searchTerms
    );

    fetchAirlines(
      '' + itemsPerPage,
      '' + 1,
      sort,
      filter,
      '&search_term=' + searchTerms
    );
    setCurrentPage(1);
  };

  return (
    <React.Fragment>
      {loading ? (
        <Container>
          <LinearProgress />
        </Container>
      ) : (
        <Container>
          <Grid container>
            <div role="presentation">
              <Breadcrumbs aria-label="breadcrumb">
                <Link color="inherit" href="/">
                  Home
                </Link>
                <Link
                  color="inherit"
                  href={'/airlines/limit=' + itemsPerPage + '&page=1'}
                >
                  Airlines
                </Link>
              </Breadcrumbs>
            </div>
          </Grid>

          <Grid container spacing={'15'} sx={{marginTop: 2, marginBottom: 2}}>
            <Grid item justify-items="start">
              <TextField
                id="search-box"
                label="Search"
                sx={{height: 40, minWidth: 250}}
                // onSubmit={handleSearch}
              ></TextField>
              <Button
                variant="contained"
                sx={{marginLeft: 1, height: 55}}
                onClick={handleSearch}
              >
                Search
              </Button>
            </Grid>

            <Grid item sx={{minWidth: 140}} justify-items="end">
              <FormControl fullWidth>
                <InputLabel id="sort-label">Sort By</InputLabel>
                <Select labelId="sort-label" label="Sort By">
                  <MenuItem
                    component={Link}
                    href={
                      currentLink.includes('&sort_by=')
                        ? currentLink.replace(sort, '')
                        : currentLink
                    }
                  >
                    None
                  </MenuItem>
                  <MenuItem
                    component={Link}
                    href={
                      currentLink.includes('&sort_by=')
                        ? currentLink.replace(sort, '&sort_by=name')
                        : currentLink + '&sort_by=name'
                    }
                  >
                    Name (asc.)
                  </MenuItem>
                  <MenuItem
                    component={Link}
                    href={
                      currentLink.includes('&sort_by=')
                        ? currentLink.replace(sort, '&sort_by=name&reverse')
                        : currentLink + '&sort_by=name&reverse'
                    }
                  >
                    Name (desc.)
                  </MenuItem>

                  <MenuItem
                    component={Link}
                    href={
                      currentLink.includes('&sort_by=')
                        ? currentLink.replace(sort, '&sort_by=country')
                        : currentLink + '&sort_by=country'
                    }
                  >
                    Operating Country (asc.)
                  </MenuItem>
                  <MenuItem
                    component={Link}
                    href={
                      currentLink.includes('&sort_by=')
                        ? currentLink.replace(sort, '&sort_by=country&reverse')
                        : currentLink + '&sort_by=country&reverse'
                    }
                  >
                    Operating Country (desc.)
                  </MenuItem>

                  <MenuItem
                    component={Link}
                    href={
                      currentLink.includes('&sort_by=')
                        ? currentLink.replace(sort, '&sort_by=founded')
                        : currentLink + '&sort_by=founded'
                    }
                  >
                    Date Founded (asc.)
                  </MenuItem>
                  <MenuItem
                    component={Link}
                    href={
                      currentLink.includes('&sort_by=')
                        ? currentLink.replace(sort, '&sort_by=founded&reverse')
                        : currentLink + '&sort_by=founded&reverse'
                    }
                  >
                    Date Founded (desc.)
                  </MenuItem>

                  <MenuItem
                    component={Link}
                    href={
                      currentLink.includes('&sort_by=')
                        ? currentLink.replace(sort, '&sort_by=fleet_size')
                        : currentLink + '&sort_by=fleet_size'
                    }
                  >
                    Fleet Size (asc.)
                  </MenuItem>
                  <MenuItem
                    component={Link}
                    href={
                      currentLink.includes('&sort_by=')
                        ? currentLink.replace(
                            sort,
                            '&sort_by=fleet_size&reverse'
                          )
                        : currentLink + '&sort_by=fleet_size&reverse'
                    }
                  >
                    Fleet Size (desc.)
                  </MenuItem>
                </Select>
              </FormControl>
            </Grid>

            <Grid item sx={{minWidth: 140}} justify-items="end">
              <FormControl fullWidth>
                <InputLabel id="filter-label">Country</InputLabel>
                <Select labelId="filter-label" label="filter">
                  {countries.map(country => (
                    <MenuItem
                      key={country}
                      component={Link}
                      href={
                        currentLink.includes(`${country},`)
                          ? currentLink.replace(`${country},`, '')
                          : currentLink.includes('&country=')
                          ? currentLink + `${country},`
                          : currentLink + `&country=${country},`
                      }
                    >
                      {country}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
          </Grid>

          <Grid container paddingBottom={2} justifyContent="left">
            <Grid item>
              <ModelPagination
                itemsPerPage={itemsPerPage}
                totalItems={airlinesFound}
                handleChange={handleChange}
                page={currentPage}
              />
            </Grid>
          </Grid>

          <Grid container spacing={'15'}>
            {airlines.map(airline => (
              <Grid item xs={3} key={airline.id}>
                <AirlineModelCard
                  id={airline.id}
                  airline_name={airline.airline_name}
                  airline_logo={airline.airline_logo}
                  airline_fleet_size={airline.airline_fleet_size}
                  airline_founded={airline.airline_founded}
                  airline_hub_code={airline.airline_hub_code}
                  airline_iata={airline.airline_iata}
                  airline_icao={airline.airline_icao}
                  airline_country={airline.airline_country}
                  toHighlight={toHighlight}
                  searchTerm={search}
                />
              </Grid>
            ))}
          </Grid>
          <Grid container paddingTop={2} justifyContent="left">
            <Typography variant="inherit">
              {airlinesFound} airlines found
            </Typography>
          </Grid>
        </Container>
      )}
    </React.Fragment>
  );
};

export default Airlines;
