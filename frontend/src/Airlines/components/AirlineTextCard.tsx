import * as React from 'react';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';

interface CardInfo {
  height: number;
  width: number;
  cardTitle: string;
  airline_av_fleet: number;
  airline_country: string;
  airline_fleet_size: number;
  airline_founded: number;
  airline_hub_code: string;
  airline_iata: string;
  airline_icao: string;
  airline_logo: string;
  airline_hub_id: number;
}

export default function AirlineTextCard(props: CardInfo) {
  let logo: string = props.airline_logo;

  // Check if a logo exists or not, return logo not found if not.
  if (logo === null) {
    logo =
      'https://user-images.githubusercontent.com/24848110/33519396-7e56363c-d79d-11e7-969b-09782f5ccbab.png';
  }

  return (
    <CardContent>
      <Typography variant="h4" component="div">
        {props.cardTitle}
        <Divider variant={'fullWidth'} sx={{marginTop: 2, marginBottom: 2}} />
      </Typography>
      <Typography variant="h6">
        {'Founded: ' + props.airline_founded}
        <br />
        {'Main Country of Operation: ' + props.airline_country}
        <br />
        {'Fleet Size: ' + props.airline_fleet_size}
        <br />
        {'Main Operating Hub: ' + props.airline_hub_code}
        <br />
        {'IATA: ' + props.airline_iata}
        <br />
        {'ICAO: ' + props.airline_icao}
        <br />
      </Typography>
    </CardContent>
  );
}
