import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, {SelectChangeEvent} from '@mui/material/Select';
import {useHistory} from 'react-router-dom';
import React from 'react';

export default function FilterBoxCountries(props) {
  const [country, setcountry] = React.useState('');
  const history = useHistory();
  const currentLink = window.location.href.substring(
    window.location.href.lastIndexOf('/') + 1
  );
  const currentPage = props;

  const handleChange = (event: SelectChangeEvent) => {
    const {
      target: {value},
    } = event;

    if (window.location.href.includes('&country=')) {
      if (country === 'All') {
        setcountry('');
      }
      let newLink = currentLink.replace(
        '&country=' + country,
        '&country=' + value
      );
      newLink = newLink.replace('&page=' + currentPage, '&page=1');
      history.push(newLink);
      setcountry(value);
    } else {
      history.push(currentLink + '&country=' + `${value}`);
      setcountry(value);
    }
  };

  return (
    <Box sx={{minWidth: 120}}>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">Country</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={country}
          label="country"
          onChange={handleChange}
        >
          <MenuItem value={'UnitedStates'}>United States</MenuItem>
          <MenuItem value={'China'}>China</MenuItem>
          <MenuItem value={'Germany'}>Germany</MenuItem>
          <MenuItem value={'Ireland'}>Ireland</MenuItem>
          <MenuItem value={'UnitedArabEmirates'}>United Arab Emirates</MenuItem>
          <MenuItem value={'UnitedKingdom'}>United Kingdom</MenuItem>
          <MenuItem value={'All'}>All</MenuItem>
        </Select>
      </FormControl>
    </Box>
  );
}
