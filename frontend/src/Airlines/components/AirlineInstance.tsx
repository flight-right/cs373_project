import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {Container, Divider, Grid, Typography, Avatar} from '@mui/material';
import {RouteComponentProps} from 'react-router-dom';
import {Button} from '@mui/material';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import AirlineTextCard from './AirlineTextCard';
import DataTable from './flightTable';
import CircularProgress from '@mui/material/CircularProgress';
import AirportTable from './airportTable';
import ImgMediaCard from './NewsCard';
import {useHistory} from 'react-router-dom';

export interface Airport {
  airport_address: string;
  airport_city: string;
  airport_flight_volume: number;
  airport_iata: string;
  airport_icao: string;
  airport_lat: number;
  airport_long: number;
  airport_maps: string;
  airport_name: string;
  airport_phone_number: string;
  airport_rating: number;
  airport_runway_number: number;
  airport_state: string;
  airport_timezone: string;
  airport_twitter: string;
  airport_website: string;
  id: number;
}

export interface Flight {
  flight_aircraft_img: string;
  flight_airline_name: string;
  flight_arr_airport: string;
  flight_arr_airport_iata: string;
  flight_arr_airport_icao: string;
  flight_arr_time: string;
  flight_dept_airport: string;
  flight_dept_airport_iata: string;
  flight_dept_airport_icao: string;
  flight_dept_time: string;
  flight_iata: string;
  flight_icao: string;
  flight_lat: number;
  flight_long: number;
  flight_number: string;
  flight_status: string;
  id: number;
}

export interface Airline {
  airline_name: string;
  airline_country: string;
  airline_logo: string;
  airline_av_fleet: number;
  airline_fleet_size: number;
  airline_founded: number;
  airline_hub_code: string;
  airline_iata: string;
  airline_icao: string;
  airline_news_img: string;
  airline_news_text: string;
  airline_news_url: string;
  flights: Flight[];
  airports: Airport[];
  id: number;
}

// const defaultAirlines: Airline[] = [];
// const defaultFlights: Flight[] = [];

const AirlineInstance = ({match}: RouteComponentProps<{id?: string}>) => {
  const [airline, setAirline] = useState<Airline>();
  const history = useHistory();
  // const [flights, setFlights]: [Flight[], (flights: Flight[]) => void] =
  //   useState(defaultFlights);

  useEffect(() => {
    fetchAirline();
    // fetchFlights();
  }, []);

  const fetchAirline = () => {
    axios
      .get<Airline>(`https://api.flightright.me/airlines/${match.params.id}`, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .then(res => {
        console.log(res);
        setAirline(res.data);
      })
      .catch(err => {
        console.log(err);
      });
  };

  return (
    <React.Fragment>
      <Container>
        {console.info(airline)}
        <Button onClick={() => history.goBack()}>
          <ArrowBackIcon /> Back to Airlines
        </Button>
        {/* {currentAirline.map(airline => {
            return (
              <Typography
                variant={'h3'}
                key={airline.id}
                sx={{paddingBottom: 1}}
                align="center"
              >
                {airline.airline_name}
              </Typography>
            );
          })} */}
        {airline ? (
          <React.Fragment>
            <Grid container justifyContent="center">
              <Grid item sx={{paddingRight: 2, paddingBottom: 2}}>
                <Avatar
                  alt="Airline Logo"
                  src={airline.airline_logo}
                  sx={{
                    width: 75,
                    height: 75,
                  }}
                />
              </Grid>

              <Grid item>
                <Typography
                  variant={'h3'}
                  // key={airline.id}
                  sx={{paddingBottom: 1, paddingTop: 1}}
                  align="center"
                >
                  {airline.airline_name}
                </Typography>
              </Grid>
            </Grid>
            <Divider variant={'fullWidth'} sx={{marginBottom: 2}} />
            <Grid container spacing={'5'}>
              <Grid item>
                <AirlineTextCard
                  key={airline.airline_name}
                  width={897}
                  height={500}
                  cardTitle={'Airline Information: '}
                  airline_av_fleet={airline.airline_av_fleet}
                  airline_country={airline.airline_country}
                  airline_fleet_size={airline.airline_fleet_size}
                  airline_founded={airline.airline_founded}
                  airline_hub_code={airline.airline_hub_code}
                  airline_iata={airline.airline_iata}
                  airline_icao={airline.airline_icao}
                  airline_logo={airline.airline_logo}
                  airline_hub_id={4}
                />
              </Grid>
              <AirportTable rows={airline.airports} />
              <DataTable rows={airline.flights} />
            </Grid>
            <Grid
              container
              justifyContent="center"
              spacing="15"
              sx={{paddingTop: 2}}
            >
              <Grid item>
                <ImgMediaCard
                  newsImg={airline.airline_news_img}
                  newsText={airline.airline_news_text}
                  newsLink={airline.airline_news_url}
                />
              </Grid>
            </Grid>
          </React.Fragment>
        ) : (
          <CircularProgress />
        )}
      </Container>
    </React.Fragment>
  );
};
export default AirlineInstance;
