import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import {Grid} from '@mui/material';

interface CardInfo {
  newsText: string;
  newsImg: string;
  newsLink: string;
}

export default function ImgMediaCard(props: CardInfo) {
  const newsText =
    props.newsText === null ? 'News Text Failed to Load' : props.newsText;

  return (
    <Card sx={{width: '100%'}}>
      <Grid container>
        <Grid item xs={2}>
          <CardMedia
            component="img"
            alt="News"
            height="300"
            image={props.newsImg}
          />
        </Grid>
        <Grid item xs={8}>
          <CardContent>
            <Typography gutterBottom variant="h6" component="div">
              Latest News
            </Typography>
            <Typography gutterBottom variant="body1" component="div">
              {newsText}
            </Typography>
          </CardContent>
        </Grid>
      </Grid>
      <CardActions sx={{paddingLeft: '90%'}}>
        <Button
          size="large"
          onClick={e => {
            e.preventDefault();
            window.location.href = props.newsLink;
          }}
        >
          Learn More
        </Button>
      </CardActions>
    </Card>
  );
}
