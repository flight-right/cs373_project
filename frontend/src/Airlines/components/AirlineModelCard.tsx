import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import {Button, CardActions} from '@mui/material';
import {Link} from 'react-router-dom';
import Highlighter from 'react-highlight-words';

type CardInfo = {
  airline_name: string;
  airline_country: string;
  airline_fleet_size: number;
  airline_founded: number;
  airline_hub_code: string;
  airline_iata: string;
  airline_icao: string;
  airline_logo: string;
  id: number;
  toHighlight: boolean;
  searchTerm: string;
};

export default function AirlineModelCard(props: CardInfo) {
  let logo: string = props.airline_logo;

  // Check if a logo exists or not, return logo not found if not.
  if (logo === null) {
    logo =
      'https://user-images.githubusercontent.com/24848110/33519396-7e56363c-d79d-11e7-969b-09782f5ccbab.png';
  }

  const search = props.searchTerm.includes('&search_term=')
    ? props.searchTerm.replace('&search_term=', '')
    : props.searchTerm;
  // console.log('searching: ' + search);

  return props.toHighlight ? (
    <Card sx={{maxWidth: 500}}>
      <CardMedia
        sx={{objectFit: 'none'}}
        component="img"
        height="100"
        image={logo}
        alt="AIRLINE_LOGO"
      />
      <CardContent>
        <Typography variant="h6" component="div">
          <Highlighter
            searchWords={[search]}
            textToHighlight={props.airline_name}
          />
        </Typography>

        <Typography variant="body1" color="text.primary">
          <Highlighter
            searchWords={[search]}
            textToHighlight={'Country: ' + props.airline_country}
          />
        </Typography>

        <Typography variant="body1" color="text.primary">
          <Highlighter
            searchWords={[search]}
            textToHighlight={'Founded: ' + props.airline_founded}
          />
        </Typography>

        <Typography variant="body1" color="text.primary">
          <Highlighter
            searchWords={[search]}
            textToHighlight={'Fleet Size: ' + props.airline_fleet_size}
          />
        </Typography>

        <Typography variant="body1" color="text.primary">
          <Highlighter
            searchWords={[search]}
            textToHighlight={'IATA: ' + props.airline_iata}
          />
        </Typography>

        <Typography variant="body1" color="text.primary">
          <Highlighter
            searchWords={[search]}
            textToHighlight={'ICAO: ' + props.airline_icao}
          />
        </Typography>

        <Typography variant="body1" color="text.primary">
          <Highlighter
            searchWords={[search]}
            textToHighlight={'Operating Hub: ' + props.airline_hub_code}
          />
        </Typography>
      </CardContent>
      <CardActions>
        <Button
          size="large"
          color="primary"
          component={Link}
          to={`/airlines/view/${props.id}`}
        >
          View
        </Button>
      </CardActions>
    </Card>
  ) : (
    <Card sx={{maxWidth: 500}}>
      <CardMedia
        sx={{objectFit: 'none'}}
        component="img"
        height="100"
        image={logo}
        alt="AIRLINE_LOGO"
      />
      <CardContent>
        <Typography variant="h6" component="div">
          {props.airline_name}
        </Typography>
        <Typography variant="body1" color="text.primary">
          Country: {props.airline_country}
        </Typography>
        <Typography variant="body1" color="text.primary">
          Founded: {props.airline_founded}
        </Typography>
        <Typography variant="body1" color="text.primary">
          Fleet Size: {props.airline_fleet_size}
        </Typography>
        <Typography variant="body1" color="text.primary">
          IATA: {props.airline_iata}
        </Typography>
        <Typography variant="body1" color="text.primary">
          ICAO: {props.airline_icao}
        </Typography>
        <Typography variant="body1" color="text.primary">
          Operating Hub: {props.airline_hub_code}
        </Typography>
      </CardContent>
      <CardActions>
        <Button
          size="large"
          color="primary"
          component={Link}
          to={`/airlines/view/${props.id}`}
        >
          View
        </Button>
      </CardActions>
    </Card>
  );
}
