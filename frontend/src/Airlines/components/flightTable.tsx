import * as React from 'react';
import {DataGrid, GridColDef} from '@mui/x-data-grid';
import {Flight} from './AirlineInstance';
import {Link} from 'react-router-dom';
import Button from '@mui/material/Button';

// // From https://reactrouter.com/web/api/Link
// function handleClick(props: string) {
//     <Link to='flightright.me/'
//   }

const columns: GridColDef[] = [
  {field: 'flight_iata', headerName: 'Upcoming Flights', width: 175},
  {field: 'flight_arr_time', headerName: 'Date', width: 175},
  {
    field: 'flight_dept_airport',
    headerName: 'Departs',
    width: 140,
  },
  {
    field: 'flight_dept_airport_iata',
    headerName: '',
    width: 50,
  },
  {field: '', headerName: '', width: 25},
  {field: 'flight_arr_airport', headerName: 'Arrives', width: 140},
  {field: 'flight_arr_airport_iata', headerName: '', width: 50},
  {field: ' ', headerName: '', width: 25},
  {field: 'flight_status', headerName: 'Status', width: 120},
  {
    field: 'id',
    headerName: 'Link',
    width: 120,
    renderCell: cellValues => {
      return (
        <Button>
          <Link to={`../../flights/${cellValues.id}`}>View</Link>
        </Button>
      );
    },
  },
  //   {field: 'firstName', headerName: 'First name', width: 130},
  //   {field: 'lastName', headerName: 'Last name', width: 130},
  //   {
  //     field: 'age',
  //     headerName: 'Age',
  //     type: 'number',
  //     width: 90,
  //   },
  //   {
  //     field: 'fullName',
  //     headerName: 'Full name',
  //     description: 'This column has a value getter and is not sortable.',
  //     sortable: false,
  //     width: 160,
  //     valueGetter: (params: GridValueGetterParams) =>
  //       `${params.getValue(params.id, 'firstName') || ''} ${
  //         params.getValue(params.id, 'lastName') || ''
  //       }`,
  //   },
];

// const rows = [
//   {id: 1, lastName: 'Snow', firstName: 'Jon', age: 35},
//   {id: 2, lastName: 'Lannister', firstName: 'Cersei', age: 42},
//   {id: 3, lastName: 'Lannister', firstName: 'Jaime', age: 45},
//   {id: 4, lastName: 'Stark', firstName: 'Arya', age: 16},
//   {id: 5, lastName: 'Targaryen', firstName: 'Daenerys', age: null},
//   {id: 6, lastName: 'Melisandre', firstName: null, age: 150},
//   {id: 7, lastName: 'Clifford', firstName: 'Ferrara', age: 44},
//   {id: 8, lastName: 'Frances', firstName: 'Rossini', age: 36},
//   {id: 9, lastName: 'Roxie', firstName: 'Harvey', age: 65},
// ];

interface FlightTableProps {
  rows: Flight[];
}

export default function DataTable(props: FlightTableProps) {
  return (
    <div style={{height: 400, width: '100%'}}>
      <DataGrid
        rows={props.rows}
        columns={columns}
        pageSize={5}
        rowsPerPageOptions={[5]}
      />
    </div>
  );
}
