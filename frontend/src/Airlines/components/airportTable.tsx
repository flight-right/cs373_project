import * as React from 'react';
import {DataGrid, GridColDef} from '@mui/x-data-grid';
import {Airport} from './AirlineInstance';
import {Link} from 'react-router-dom';
import Button from '@mui/material/Button';

const columns: GridColDef[] = [
  {field: 'airport_name', headerName: 'Operating Airports', width: 250},
  {field: 'airport_iata', headerName: 'IATA', width: 250},
  {
    field: '',
    headerName: 'Link',
    width: 150,
    renderCell: cellValues => {
      return (
        <Button>
          <Link to={`../../airports/${cellValues.id}`}>View</Link>
        </Button>
      );
    },
  },
];

interface FlightTableProps {
  rows: Airport[];
}

export default function AirportTable(props: FlightTableProps) {
  return (
    <div style={{height: 400, width: 745, marginBottom: 5}}>
      <DataGrid
        rows={props.rows}
        columns={columns}
        pageSize={5}
        rowsPerPageOptions={[5]}
      />
    </div>
  );
}
