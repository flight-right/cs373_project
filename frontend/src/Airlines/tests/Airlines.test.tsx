import React from 'react';
import Airlines from '../index';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route} from 'react-router-dom';
import AirlineInstance from '../components/AirlineInstance';

it('loads model page(s) without issue', () => {
  const div = document.createElement('div');

  ReactDOM.render(
    <BrowserRouter>
      <Route exact path="/airlines/limit=9&page=1" component={Airlines} />
    </BrowserRouter>,
    div
  );
  expect(div.textContent).not.toBe(null);
});

it('loads instance pages without issue', () => {
  const div = document.createElement('div');

  ReactDOM.render(
    <BrowserRouter>
      <Route exact path="/airlines/:id" component={AirlineInstance} />
    </BrowserRouter>,
    div
  );
  expect(div.textContent).not.toBe(null);
});

it('loads model page cards for sorting without issue', () => {
  const div = document.createElement('div');

  ReactDOM.render(
    <BrowserRouter>
      <Route
        exact
        path="/airlines/limit=9&page=1&sort_by=name"
        component={Airlines}
      />
    </BrowserRouter>,
    div
  );
  expect(div.textContent).not.toBe(null);
});

it('loads model page cards for filtering without issue', () => {
  const div = document.createElement('div');

  ReactDOM.render(
    <BrowserRouter>
      <Route
        exact
        path="/airlines/limit=9&page=1&country=china"
        component={Airlines}
      />
    </BrowserRouter>,
    div
  );
  expect(div.textContent).not.toBe(null);
});

it('loads model page cards for filtering AND sorting without issue', () => {
  const div = document.createElement('div');

  ReactDOM.render(
    <BrowserRouter>
      <Route
        exact
        path="/airlines/limit=9&page=1&sort_by=name&country=china"
        component={Airlines}
      />
    </BrowserRouter>,
    div
  );
  expect(div.textContent).not.toBe(null);
});
