import * as React from 'react';
import {BrowserRouter, Route} from 'react-router-dom';

import Navbar from './components/navbar';
import Footer from './components/footer';

import Home from './Splash';
import About from './About';

import Search from './Search';
import SearchResults from './Search/SearchResults';

import Airlines from './Airlines';
import AirlineInstance from './Airlines/components/AirlineInstance';

import Flights from './Flights';

import Airports from './Airports';
import AirportInstance from './Airports/components/AirportInstance';
import Visual from './Visual';
import ProviderVisual from './ProviderVisual';

// need to import airport instances

function App() {
  return (
    <>
      <Navbar />
      <BrowserRouter>
        <Route exact path="/" component={Home} />
        <Route exact path="/about" component={About} />

        <Route exact path="/visual" component={Visual} />
        <Route exact path="/provider" component={ProviderVisual} />
        {/*<Route exact path="/visuals" component={Visual} />*/}
        {/* <Route
          exact
          path="/airlines/limit=:l&page=:p&sort_by=:s"
          component={Airlines}
        /> */}
        <Route path="/airlines/limit=:limit&page=:page" component={Airlines} />
        <Route exact path="/airlines/view/:id" component={AirlineInstance} />

        <Route path="/flights" component={Flights} />

        <Route exact path="/airports/view/:id" component={AirportInstance} />
        <Route path="/airports/limit=:limit&page=:page" component={Airports} />

        <Route exact path="/search" component={Search} />
        <Route exact path="/search/:searchTerms" component={SearchResults} />
      </BrowserRouter>
      <Footer />
    </>
  );
}

export default App;
