import React from 'react';
import {Container} from '@mui/material';

import BreedVisual from './components/BreedVisual';
import {StateVisual} from './components/StateVisual';
import {CountryVisual} from './components/CountryVisual';
// import {parseVisData} from './Filtering';

// Function to parse data for visualisations.
export const parseVisData = (data, d3) => {
  const list: Array<{
    label: string;
    name: string;
    value: number;
    fill: string;
  }> = [];
  const tempMap = new Map();
  /* Creating a map that contains each unique attribute,
     and increments the count associated with how many instances are present. */
  for (const key in data) {
    tempMap.set(
      data[key],
      typeof tempMap.get(data[key]) !== 'undefined'
        ? tempMap.get(data[key]) + 1
        : 1
    );
  }
  /* Processing the data into a suitable format for the
     imported visualisation components in OurViz.js and TheirViz.js */
  tempMap.forEach((value, key) => {
    const obj: {label: string; name: string; value: number; fill: string} = {
      label: '',
      name: '',
      value: 0,
      fill: '',
    };
    if (key !== undefined) {
      // Used to identify if data is for a d3 visualisation component or a recharts one.
      if (d3) {
        obj['label'] = key;
      } else {
        obj['name'] = key;
      }
      obj['value'] = value;
      obj['fill'] =
        '#' + (0x1000000 + Math.random() * 0xffffff).toString(16).substr(1, 6);
      list.push(obj);
    }
  });
  // Sorting the array of object by the count values in descending order.
  // Desceding order specifically useful for funnel chart.
  list.sort((a, b) => b.value - a.value);
  return list;
};

function Visual() {
  return (
    <Container sx={{mt: 4, mb: 4}}>
      <BreedVisual />
      <CountryVisual />
      <StateVisual />
    </Container>
  );
}

export default Visual;
