import React, {useEffect, useState} from 'react';

import {Box, CircularProgress} from '@material-ui/core';
import axios from 'axios';

import {
  ResponsiveContainer,
  Bar,
  BarChart,
  YAxis,
  XAxis,
  Tooltip,
} from 'recharts';

interface IResponse {
  page: IAirline[];
}

interface IAirline {
  breed_name: string;
}

const getAirlinesInfo = async () => {
  const fleets = {};
  for (let i = 1; i <= 5; i++) {
    await axios
      .get<IResponse>(`https://api.adoptapet.me/sb?page=${i}`)
      .then(response => {
        console.log(response);
        response.data.page.forEach((airline: IAirline) => {
          if (!fleets[airline.breed_name])
            fleets[airline.breed_name] = {name: airline.breed_name, count: 0};
          // fleets[airline.breed_name].name = airline.breed_name;
          fleets[airline.breed_name].count += 1;
        });
      })
      .catch(ex => {
        console.log(ex);
        // const error =
        //   ex.response.status === 404
        //     ? 'Resource Not found'
        //     : 'An unexpected error has occurred';
      });
  }

  const res = Object.values(fleets);

  return res;
};

function FRVisuals() {
  const [fleets, setFleets]: [any[], (fleets: any[]) => void] = useState<
    number[]
  >([]);
  const [loading, setLoading]: [boolean, (loading: boolean) => void] =
    React.useState<boolean>(true);

  useEffect(() => {
    let cancel = false;

    const fetchData = async () => {
      if (cancel) {
        return;
      }
      const fleetsResponse = await getAirlinesInfo();
      console.log(fleetsResponse);
      setFleets(fleetsResponse);
      setLoading(false);
    };

    fetchData();

    return () => {
      setFleets([]);
      cancel = true;
    };
  }, []);

  if (loading) {
    return <CircularProgress />;
  }

  return (
    <Box display="flex" flexDirection="column" alignItems="center" width={1000}>
      <ResponsiveContainer width="90%" height={600}>
        <BarChart width={600} height={500} data={fleets}>
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Bar dataKey={'count'} fill="#8884d8" />
        </BarChart>
      </ResponsiveContainer>
    </Box>
  );
}

export default FRVisuals;
