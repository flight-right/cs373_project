import React, {useEffect, useState} from 'react';
import axios from 'axios';
import {Cell, Pie, PieChart, Tooltip} from 'recharts';
import {parseVisData} from '../index';

export function CountryVisual() {
  const [originData, setOriginData] = useState<any>([]);
  // const [statesData, setStatesData] = useState<any>([]);

  useEffect(() => {
    /**
     * Parses API data and returns formatted data for visualization
     * @param {List[AdoptablePets]} data
     * @return {
     *    breed_name: String (Pet breed)
     *    value: <Value> (number of breed)
     * }
     */
    // Getting data on component load.
    const getData = async () => {
      // const breeds: any = await axios.get(
      //   'https://api.adoptapet.me/ap?breednames&page=-1'
      // );
      const origins: any = await axios.get(
        'https://api.adoptapet.me/sb?origins&page=-1'
      );
      // const states: any = await axios.get(
      //   'https://api.adoptapet.me/ac?states&page=-1'
      // );
      // const tempBreeds = breeds.data.page.map(el => el.breed_name);
      const tempOrigins = origins.data.page.map(el => el.origin);
      // const tempStates = states.data.page.map(el => el.state);
      // const parsedData = parseVisData(tempBreeds, true);
      const originsParsedData = parseVisData(tempOrigins, false);
      // const statesParsedData = parseVisData(tempStates, false);
      // setData(parsedData);
      setOriginData(originsParsedData);
      // setStatesData(statesParsedData);
    };
    getData();
  }, []);

  return (
    <div>
      <h2>Species Origin Countries</h2>
      {/* Using the pie chart from recharts and providing required props */}
      <PieChart width={1000} height={1000}>
        <Pie
          data={originData}
          dataKey="value"
          nameKey="name"
          cx="50%"
          cy="50%"
          outerRadius={400}
          label
          labelLine
        >
          {(entry, index) => <Cell key={`cell-${index}`} fill={entry.fill} />}
        </Pie>
        <Tooltip />
      </PieChart>
    </div>
  );
}
