import React, {useEffect, useState} from 'react';
import axios from 'axios';
import {Tooltip, Treemap} from 'recharts';
import {parseVisData} from '../index';

export function StateVisual() {
  // const [originData, setOriginData] = useState<any>([]);
  const [statesData, setStatesData] = useState<any>([]);

  useEffect(() => {
    /**
     * Parses API data and returns formatted data for visualization
     * @param {List[AdoptablePets]} data
     * @return {
     *    breed_name: String (Pet breed)
     *    value: <Value> (number of breed)
     * }
     */
    // Getting data on component load.
    const getData = async () => {
      // const breeds: any = await axios.get(
      //   'https://api.adoptapet.me/ap?breednames&page=-1'
      // );
      // const origins: any = await axios.get(
      //   'https://api.adoptapet.me/sb?origins&page=-1'
      // );
      const states: any = await axios.get(
        'https://api.adoptapet.me/ac?states&page=-1'
      );
      // const tempBreeds = breeds.data.page.map(el => el.breed_name);
      // const tempOrigins = origins.data.page.map(el => el.origin);
      const tempStates = states.data.page.map(el => el.state);
      // const parsedData = parseVisData(tempBreeds, true);
      // const originsParsedData = parseVisData(tempOrigins, false);
      const statesParsedData = parseVisData(tempStates, false);
      // setData(parsedData);
      // setOriginData(originsParsedData);
      setStatesData(statesParsedData);
    };
    getData();
  }, []);

  return (
    <div>
      <h2>Adoption Center State</h2>
      <Treemap
        width={500}
        height={500}
        data={statesData}
        dataKey={'value'}
        // ratio={4 / 3}
        stroke={'#fff'}
        fill={'#000'}
      >
        <Tooltip />
      </Treemap>
    </div>
  );
}
