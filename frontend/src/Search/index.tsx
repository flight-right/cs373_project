import React from 'react';
import {Container, TextField, Button, Grid} from '@mui/material';
import {Link, Box, Typography} from '@mui/material';
import {useHistory} from 'react-router-dom';
import FetchAirlines from './SearchResults/components/fetchAirlines';
import FetchAirports from './SearchResults/components/fetchAirports';
import FetchFlights from './SearchResults/components/fetchFlights';

function Splash() {
  const history = useHistory();

  // Handles the searching for universal page
  const handleSearch = () => {
    const searchTerms =
      document.getElementById('search-box') === null
        ? ''
        : (document.getElementById('search-box')! as HTMLInputElement).value;

    history.push(window.location.pathname + '/' + searchTerms);
  };

  return (
    <>
      <Box
        sx={{
          minHeight: 300,
          display: 'flex',
          alignItems: 'center',
        }}
      >
        <Container>
          <Typography
            variant={'h2'}
            sx={{
              fontWeight: 'bold',
            }}
          >
            Find Your Flight Data
          </Typography>
          <Box
            sx={{
              minHeight: 10,
              display: 'flex',
              alignItems: 'center',
            }}
          ></Box>
          <TextField
            id="search-box"
            label="Search"
            sx={{height: 40, minWidth: 1050}}
            // onSubmit={handleSearch}
          ></TextField>
          <Button
            variant="contained"
            sx={{marginLeft: 1, height: 55}}
            component={Link}
            onClick={handleSearch}
          >
            Search
          </Button>
        </Container>
      </Box>

      <Box
        sx={{
          minHeight: 200,
          display: 'flex',
          alignItems: 'center',
        }}
      >
        <Container>
          <Box
            sx={{
              minHeight: 50,
              display: 'flex',
              alignItems: 'center',
            }}
          ></Box>
          <Typography
            variant={'h3'}
            sx={{
              fontWeight: 'bold',
            }}
          >
            Suggestions:
          </Typography>
          <Grid container spacing={4} sx={{mb: 4, mt: 1, mx: 1}}>
            <Grid item>
              <Typography
                variant={'h5'}
                sx={{
                  fontWeight: 'bold',
                }}
              >
                Flights
              </Typography>
              <FetchFlights search={''} limit={3} />
            </Grid>

            <Grid item>
              <Typography
                variant={'h5'}
                sx={{
                  fontWeight: 'bold',
                }}
              >
                Airlines
              </Typography>
              <FetchAirlines search={''} limit={4} />
            </Grid>

            <Grid item>
              <Typography
                variant={'h5'}
                sx={{
                  fontWeight: 'bold',
                }}
              >
                Airports
              </Typography>
              <FetchAirports search={''} limit={4} />
            </Grid>
          </Grid>
        </Container>
      </Box>
    </>
  );
}

export default Splash;
