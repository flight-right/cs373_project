import React from 'react';
import Search from '../index';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route} from 'react-router-dom';
import SearchResults from '../SearchResults';

it('loads search bar component without issue', () => {
  const div = document.createElement('div');

  ReactDOM.render(
    <BrowserRouter>
      <Route exact path="/search" component={Search} />
    </BrowserRouter>,
    div
  );
  expect(div.textContent).not.toBe(null);
});

it('loads search page without issue', () => {
  const div = document.createElement('div');

  ReactDOM.render(
    <BrowserRouter>
      <Route exact path="/search/:searchTerm" component={SearchResults} />
    </BrowserRouter>,
    div
  );
  expect(div.textContent).not.toBe(null);
});
