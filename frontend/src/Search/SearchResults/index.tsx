import React from 'react';
import Box from '@mui/material/Box';
import {Container, Grid, Stack, Typography} from '@mui/material';

import FetchAirports from './components/fetchAirports';
import FetchAirlines from './components/fetchAirlines';
import FetchFlights from './components/fetchFlights';

function ResultsHeader() {
  const searchTerms = window.location.href.substring(
    window.location.href.lastIndexOf('/') + 1
  );

  return (
    <Box
      sx={{
        minHeight: 100,
        display: 'flex',
        alignItems: 'center',
      }}
    >
      <Container>
        <Typography
          variant={'h4'}
          sx={{
            fontWeight: 'bold',
          }}
        >
          Search results for '{searchTerms}'
        </Typography>
      </Container>
    </Box>
  );
}

function SearchResults() {
  const searchTerms = window.location.href.substring(
    window.location.href.lastIndexOf('/') + 1
  );

  return (
    <>
      <ResultsHeader />
      <Container sx={{mb: 4}}>
        <Grid container spacing={4} sx={{mb: 4, mx: 4}}>
          <Typography
            variant={'h5'}
            sx={{
              fontWeight: 'bold',
            }}
          >
            Flights
          </Typography>
          <FetchFlights search={searchTerms} limit={10} />
        </Grid>

        <Stack spacing={4} sx={{mb: 4}}>
          <Typography
            variant={'h5'}
            sx={{
              fontWeight: 'bold',
            }}
          >
            Airports
          </Typography>
          <FetchAirports search={searchTerms} limit={9} />
        </Stack>

        <Grid container spacing={4} sx={{mb: 4}}>
          <Typography
            variant={'h5'}
            sx={{
              fontWeight: 'bold',
            }}
          >
            Airlines
          </Typography>
          <FetchAirlines search={searchTerms} limit={9} />
        </Grid>
      </Container>
    </>
  );
}

export default SearchResults;
