import React, {useEffect, useState} from 'react';
// import {LinearProgress} from '@mui/material';
import {Flight} from '../../../Flights';
import FlightsDisplay from '../../../Flights/components/flightDataDisplay';
import {fetchAllFlightData, buildQuery} from '../../../Flights/fetchData';

interface SearchProps {
  search: string;
  limit: number;
}

function FetchFlights(props: SearchProps) {
  // const [loading, setLoading] = useState(false);
  const [rows, setRows] = useState<Flight[]>([]);

  const searchTerms = props.search;

  async function fetchFlights(search: string) {
    // setLoading(true);
    const params = buildQuery({
      page: 1,
      limit: props.limit,
      search_term: search,
    });
    // console.log('run query', params);

    const data = await fetchAllFlightData(params);
    setRows(data.rows);
    console.log('flights search', searchTerms);
  }

  useEffect(() => {
    fetchFlights(searchTerms);
  }, []);

  return <FlightsDisplay rows={rows} highlight={searchTerms} />;
}

export default FetchFlights;
