import React, {useEffect, useState} from 'react';
import {Grid} from '@mui/material';
import axios from 'axios';
import AirportModelCard from '../../../Airports/components/AirportModelCard';
import ModelPagination from '../../../components/ModelPagination';
import {LinearProgress} from '@mui/material';

interface AirportAPI {
  count: number;
  data: Airport[];
  total: number;
}

interface Airport {
  airport_timezone: number;
  airport_name: string;
  airport_flight_volume: number;
  airport_iata: string;
  airport_icao: string;
  airport_lat: number;
  airport_long: number;
  airport_rating: number;
  airport_website: string;
  airport_runway_number: number;
  airport_state: string;
  airport_city: string;
  airport_maps: string;
  id: number;
}

interface searchData {
  search: string;
  limit: number;
}

const defaultAirports: Airport[] = [];

const FetchAirports = (props: searchData) => {
  const [airports, setAirports]: [Airport[], (airports: Airport[]) => void] =
    useState(defaultAirports);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [itemsPerPage] = useState(props.limit);
  const [airportsFound, setAirportsFound] = useState(0);

  const searchTerms = props.search;

  useEffect(() => {
    fetchAirports(String(currentPage), searchTerms);
  }, []);

  const fetchAirports = (page: string, search: string) => {
    setLoading(true);
    axios
      .get<AirportAPI>(
        `https://api.flightright.me/airports?limit=${itemsPerPage}&page=${currentPage}&search_term=${searchTerms}`,
        {
          headers: {
            'Content-Type': 'application/json',
          },
        }
      )
      .then(res => {
        // console.log('SEARCH IS: ' + search);
        // console.log(res);
        setAirports(res.data.data);
        setLoading(false);
      })
      .catch(err => {
        console.log(err);
      });

    axios
      .get<AirportAPI>(
        `https://api.flightright.me/airports?limit=${itemsPerPage}&page=${currentPage}&search_term=${search}`,
        {
          headers: {
            'Content-Type': 'application/json',
          },
        }
      )
      .then(res => {
        setAirportsFound(res.data.count);
        setLoading(false);
      });
  };

  // Change page
  const handleChange = (
    event: React.ChangeEvent<unknown>,
    pageNumber: number
  ) => {
    fetchAirports('' + pageNumber, searchTerms);
    setCurrentPage(pageNumber);
  };

  return (
    <React.Fragment>
      {loading ? (
        <LinearProgress />
      ) : (
        <>
          <Grid container spacing="15" sx={{marginTop: 2, marginBottom: 2}}>
            <Grid container paddingBottom={2} justifyContent="left">
              <Grid item>
                <ModelPagination
                  itemsPerPage={itemsPerPage}
                  totalItems={airportsFound}
                  handleChange={handleChange}
                  page={currentPage}
                />
              </Grid>
            </Grid>

            <Grid container spacing={'15'}>
              {airports.map(airport => (
                <Grid item xs={3} key={airport.airport_name}>
                  <AirportModelCard
                    airport_photo={airport.airport_maps}
                    airport_name={airport.airport_name}
                    airport_flight_volume={airport.airport_flight_volume}
                    airport_runway_number={airport.airport_runway_number}
                    airport_iata={airport.airport_iata}
                    airport_icao={airport.airport_icao}
                    airport_id={airport.id}
                    toHighlight={true}
                    searchTerm={searchTerms}
                    airport_state={airport.airport_state}
                    airport_city={airport.airport_city}
                  />
                </Grid>
              ))}
            </Grid>
          </Grid>
        </>
      )}
    </React.Fragment>
  );
};

export default FetchAirports;
