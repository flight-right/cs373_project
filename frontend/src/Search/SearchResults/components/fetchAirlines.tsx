import React, {useEffect, useState} from 'react';
import {Grid} from '@mui/material';
import axios from 'axios';
import AirlineModelCard from '../../../Airlines/components/AirlineModelCard';
import ModelPagination from '../../../components/ModelPagination';
import {LinearProgress} from '@mui/material';

interface AirlineAPI {
  count: number;
  data: Airline[];
  total: number;
}

interface Airline {
  airline_name: string;
  airline_country: string;
  airline_fleet_size: number;
  airline_founded: number;
  airline_hub_code: string;
  airline_iata: string;
  airline_icao: string;
  airline_logo: string;
  id: number;
}

interface searchData {
  search: string;
  limit: number;
}

const defaultAirlines: Airline[] = [];

const FetchAirlines = (props: searchData) => {
  const [airlines, setAirlines]: [Airline[], (airlines: Airline[]) => void] =
    useState(defaultAirlines);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [itemsPerPage] = useState(props.limit);
  const [airlinesFound, setAirlinesFound] = useState(0);

  const searchTerms = props.search;

  useEffect(() => {
    fetchAirlines(String(currentPage), searchTerms);
  }, []);

  const fetchAirlines = (page: string, search: string) => {
    setLoading(true);
    // console.log("SEARCH IS1: " + search);
    axios
      .get<AirlineAPI>(
        `https://api.flightright.me/airlines?limit=${itemsPerPage}&page=${currentPage}&search_term=${searchTerms}`,
        {
          headers: {
            'Content-Type': 'application/json',
          },
        }
      )
      .then(res => {
        // console.log("SEARCH IS2: " + search);
        // console.log(res);
        setAirlines(res.data.data);
        setLoading(false);
      })
      .catch(err => {
        console.log(err);
      });

    axios
      .get<AirlineAPI>(
        `https://api.flightright.me/airlines?limit=${itemsPerPage}&page=${currentPage}&search_term=${search}`,
        {
          headers: {
            'Content-Type': 'application/json',
          },
        }
      )
      .then(res => {
        setAirlinesFound(res.data.count);
        setLoading(false);
      });
  };

  // Change page
  const handleChange = (
    event: React.ChangeEvent<unknown>,
    pageNumber: number
  ) => {
    fetchAirlines('' + pageNumber, searchTerms);
    setCurrentPage(pageNumber);
  };

  return (
    <React.Fragment>
      {loading ? (
        <LinearProgress />
      ) : (
        <>
          <Grid container spacing="15" sx={{marginTop: 2, marginBottom: 2}}>
            <Grid container paddingBottom={2} justifyContent="left">
              <Grid item>
                <ModelPagination
                  itemsPerPage={itemsPerPage}
                  totalItems={airlinesFound}
                  handleChange={handleChange}
                  page={currentPage}
                />
              </Grid>
            </Grid>

            <Grid container spacing={'15'}>
              {airlines.map(airline => (
                <Grid item xs={3} key={airline.airline_name}>
                  <AirlineModelCard
                    id={airline.id}
                    airline_name={airline.airline_name}
                    airline_logo={airline.airline_logo}
                    airline_fleet_size={airline.airline_fleet_size}
                    airline_founded={airline.airline_founded}
                    airline_hub_code={airline.airline_hub_code}
                    airline_iata={airline.airline_iata}
                    airline_icao={airline.airline_icao}
                    airline_country={airline.airline_country}
                    toHighlight={true}
                    searchTerm={searchTerms}
                  />
                </Grid>
              ))}
            </Grid>
          </Grid>
        </>
      )}
    </React.Fragment>
  );
};

export default FetchAirlines;
