import os
import unittest

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait

driver = None
wait = None

URL = "https://www.flightright.me"


def url_format(url: str):
    return url.strip('/')


# https://github.com/aleksandr-kotlyar/python-gitlabci-selenium/blob/master/conftest.py

class GUITests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("starting setup")

        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')

        cls.driver = webdriver.Chrome('./chromedriver', options=chrome_options)
        cls.driver.implicitly_wait(10)

        cls.wait = WebDriverWait(driver, 10)

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()

    def setUp(self):
        self.driver.get(URL)

    # def tearDown(self):
    #     self.driver.quit()

    def test_title(self):
        self.assertIn("Flight Right", self.driver.title)

    def test_about(self):
        link = self.driver.find_element_by_partial_link_text('ABOUT')
        link.click()
        self.assertEqual(url_format(self.driver.current_url), URL + "/about")

    def test_home(self):
        link = self.driver.find_element_by_partial_link_text('Flight Right')
        link.click()
        self.assertEqual(url_format(self.driver.current_url), URL)

    def test_flights(self):
        link = self.driver.find_element_by_partial_link_text('FLIGHTS')
        link.click()
        self.assertEqual(url_format(self.driver.current_url), URL + '/flights')

    def test_airlines(self):
        link = self.driver.find_element_by_partial_link_text('AIRLINES')
        link.click()
        self.assertEqual(url_format(self.driver.current_url), URL + '/airlines/limit=9&page=1')

    def test_airports(self):
        link = self.driver.find_element_by_partial_link_text('AIRPORTS')
        link.click()
        self.assertEqual(url_format(self.driver.current_url), URL + '/airports/limit=9&page=1')

    def test_flights_back(self):
        link = self.driver.find_element_by_partial_link_text('FLIGHTS')
        link.click()
        link = self.driver.find_element_by_partial_link_text('Home')
        link.click()
        self.assertEqual(url_format(self.driver.current_url), URL)

    # def test_airports_back(self):
    #     link = self.driver.find_element_by_partial_link_text('AIRPORTS')
    #     link.click()

    #     link = self.driver.find_element_by_partial_link_text('BACK TO HOME')
    #     link.click()
    #     self.assertEqual(url_format(self.driver.current_url), URL)

    # def test_airlines_back(self):
    #     link = self.driver.find_element_by_partial_link_text('AIRLINES')
    #     link.click()

    #     link = self.driver.find_element_by_partial_link_text('BACK TO HOME')
    #     link.click()
    #     self.assertEqual(url_format(self.driver.current_url), URL)

    def test_flights_home(self):
        link = self.driver.find_element_by_partial_link_text('View Flights')
        link.click()
        self.assertEqual(url_format(self.driver.current_url), URL + '/flights')

    def test_airports_home(self):
        link = self.driver.find_element_by_partial_link_text('View Airports')
        link.click()
        self.assertEqual(url_format(self.driver.current_url), URL + '/airports/limit=9&page=1')

    def test_airlines_home(self):
        link = self.driver.find_element_by_partial_link_text('View Airlines')
        link.click()
        self.assertEqual(url_format(self.driver.current_url), URL + '/airlines/limit=9&page=1')


if __name__ == '__main__':
    unittest.main()
