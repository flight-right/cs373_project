front-end-lint: front-end-prettier front-end-eslint

front-end-prettier:
	cd frontend && npx prettier --write src

front-end-eslint:
	cd frontend && npx eslint --ext js,jsx,ts,tsx --fix src

front-end-build:
	cd frontend && npm run-script build

front-end-run:
	cd frontend && npm start